#include <string>
#include <fstream>
#include <string.h>
#include <iomanip>
#include <stdio.h>
#include <stdlib.h>
#define NumOfFiles 1000

using namespace std;
typedef float Numeric;
int main(){
  
  char file_name[8];
  FILE* fd;
  sprintf(file_name, "anim");
  fd = fopen (file_name, "rb");
  if(fd == NULL)
    printf("Error openning %s \n",file_name);
  else{
    for (int i=0; i<NumOfFiles; ++i){
      int sets, obs_size, time;
      fread(&sets, sizeof(int), 1, fd);
      //printf("Size of sets: %d\n", sets);
      Numeric* data = (Numeric*) malloc(sets* 2*144*sizeof(Numeric));
      if(data==NULL){
	printf("ERROR! MAlloc data!\n");
	}
      fread(data, sets*2*144*sizeof(Numeric), 1, fd);
      /*for(int p =0; p<144; ++p){
	printf("%f,   %f\n", data[p*2], data[p*2+1]);
	}*/
      fread(&obs_size, sizeof(int), 1, fd);
      //printf("OBS_size = %d\n", obs_size);
      char* xyz_obs = (char*) malloc(obs_size * sizeof(char));
      if(xyz_obs==NULL){
	printf("ERROR! Malloc xyz_obs!\n");
      }
      fread(xyz_obs, obs_size, 1, fd);
      fread(&time, sizeof(int), 1, fd);
      //printf("Tmie: %d\n", time);
      //fclose(fd);
      char file_name[16];
      FILE* file_descriptor;
      sprintf(file_name, "anim_%d", i);
      file_descriptor = fopen (file_name, "w");
      if(file_descriptor == NULL)
	printf("Error openning %s \n",file_name);
      else{
	int index=0, local_index=0;
	for(int k=0; k<sets; ++k){

	  fprintf(file_descriptor, " %d \n   %d\n", 144+593, ((local_index+1) * 250));
	  for(int its=0; its<144; ++its){
	    //converting meter to inches
	    double x_pos = data[index + its*2]*39.3701f;
	    double y_pos = data[index + its*2 +1]*39.3701f;
	    fprintf(file_descriptor, "C%12.3f%12.3f       0.000\n", x_pos, y_pos);
	  }
	  fprintf(file_descriptor, "%s\n", xyz_obs);
	  ++local_index;
	  index += 2*144;
	}
	fclose(file_descriptor);
	}
    }
    fclose(fd);
  }
}
