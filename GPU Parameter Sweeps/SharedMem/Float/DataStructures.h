/**
 * Definition of some data types and enums.
 *
 * @author  Mehran Sadeghi Lahijani <sadeghil@cs.fsu.edu>
 * 
 */

#ifndef DataStructures_h
#define DataStructures_h

#include <vector>
#include <chrono>
#include <array>
#include <map>

#define NumberOfPassengers 144
#define NumberOfObstacles 593
#define ParametersPerSimulation 6
#define NumberOfBlocks 2000
#define NumberOfThreads 145

typedef float Numeric;
typedef std::vector<Numeric> NumericVector;




typedef std::chrono::high_resolution_clock::time_point Time;
enum PassengerTypes {FIRST_CLASS, BUSINESS, ECONOMY};
typedef PassengerTypes PassengerType;

enum PassengerAims {DOWN_TO_AISLE, UP_TO_AISLE, ALIGNING, IN_AISLE, TOWARD_EXIT, LOADONG_LUGGAGE, OUT};
typedef PassengerAims Aim;

enum PassengersRaceStatus {IN_RACE, WINNER, LOOSER, NO_RACE};




struct PassengersStruct
{
  //Numeric *mass __attribute__((aligned(32)));
    Numeric *priority __attribute__((aligned(32)));
    int *waitTime __attribute__((aligned(32)));
    Aim *aim __attribute__((aligned(32)));
    Numeric *position_x __attribute__((aligned(32)));
    Numeric *position_y __attribute__((aligned(32)));
    Numeric *velocity_x __attribute__((aligned(32)));
    Numeric *velocity_y __attribute__((aligned(32)));
    Numeric *seat_position_x __attribute__((aligned(32))); //R
    int *has_released __attribute__((aligned(32))); //bit vecor
    Numeric *desiredSpeed __attribute__((aligned(32)));
    int *ID __attribute__((aligned(32))); //  int
    Numeric *race_position_x __attribute__((aligned(32)));
    Numeric *race_position_y __attribute__((aligned(32)));
    int *race_opID __attribute__((aligned(32))); // int
    int *race_is_finished __attribute__((aligned(32))); // bit
    int *race_counter __attribute__((aligned(32)));
    int *race_status __attribute__((aligned(32)));
    Numeric *force_x __attribute__((aligned(32)));
    Numeric *force_y __attribute__((aligned(32)));
    int *nearest_ID __attribute__((aligned(32))); //
    Numeric *nearest_position_x __attribute__((aligned(32)));
    Numeric *nearest_position_y __attribute__((aligned(32)));
    Numeric *nearest_distance __attribute__((aligned(32)));
};

    
typedef PassengersStruct Passengers;



struct ObstaclesStruct{
    Numeric *positions_x __attribute__((aligned(32)));
    Numeric *positions_y __attribute__((aligned(32)));
};

typedef ObstaclesStruct Obstacles;


#endif /* DataStructures_h */

