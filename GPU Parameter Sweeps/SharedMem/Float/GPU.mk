COMMONDIR	  := /home/sadeghil/Parallel/Cuda_Tests
CUDA_INSTALL_PATH := /opt/packages/cuda/9.2

NVCC       := nvcc

CC         := icc
LINK       := icc

NVCCFLAGS  := -O3 -arch=sm_60
#NVCCFLAGS  :=  -O3
INCLUDES   := -I. -I$(CUDA_INSTALL_PATH)/include -I$(COMMONDIR)
LIBS       := -L$(CUDA_INSTALL_PATH)/lib64  -lGL -lcudart
