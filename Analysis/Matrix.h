#ifndef _MATRIX_H
#define _MATRIX_H
#include <array>
#include <stdio.h>
#include <string>
#include <vector>
#define SIZE 367
class Matrix;
typedef Matrix matrix;
typedef std::vector<matrix> matrices;

class Matrix{
 private:
  int count;
  int sum;
  long euclidean;
  //int row[SIZE]; 
  int ID;
  int type;
  std::array<int, SIZE*SIZE> Mat; //Actual matrix as an 1-D array

 public:
  Matrix(const std::array<int, SIZE*SIZE> mat, int type, int id);
  Matrix(const Matrix & mat_t);
  //  ~Matrix();
  Matrix & operator=(const Matrix &);
  int GetID() const;
  int GetType() const;
  int GetSum() const;
  int GetCount() const;
  int GetElement(int) const;
  long GetEuclidean() const;
  //int* GetRow();// const;
  std::array<int, SIZE*SIZE> GetMatrix() const;
  void printMat() const;
  //  string to_string() const;
};


/*bool operator == (const Matrix&, const Matrix &);
bool operator != (const Matrix&, const Matrix &);*/

#endif

