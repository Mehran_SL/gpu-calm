#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include <PositionsSequence.h>
#include <Positions.h>
#include <cstdlib>

using namespace std;


bool PositionsSequence::ImportPositionsSequence(const char *FileName)

{

  ifstream File(FileName);

  string s_temp;
  int TimeStep, N;
  stringstream InputFile; 

  if(!File)
    return false;
  else{
    InputFile << File.rdbuf();
    File.close();
  }

  InputFile >> N;

   while(InputFile >> TimeStep)
   {
    getline(InputFile, s_temp);  // Gets until end of line
    Data.push_back(Positions());
    Data.back().ImportPositions(N, InputFile, TimeStep);

    InputFile >> N;
  }

  LastTimeStep = TimeStep;



  return true;

}

