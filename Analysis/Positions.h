#ifndef _POSITIONS_H
#define _POSITIONS_H

#include <vector>
#include <cfloat>

using namespace std;


class Positions
{
 public:

  struct PositionStruct
  {
     float x, y, z;
     char Type;
     int TimeStep;
  };

  typedef PositionStruct Position;
  typedef vector<Position> PositionsType;

  private:

    PositionsType Humans, Surfaces;

public :

  bool ImportPositions(int N, istream &in, int timestep);
  int HumanSize(){return Humans.size();}
  int SurfaceSize(){return Surfaces.size();}


  char GetHumanClass(int i) const {return Humans[i].Type;}
  char GetSurfaceType(int i) const {return Surfaces[i].Type;}
  Position const & GetHuman(int i) const {return Humans[i];}
  Position const & GetSurface(int i) const {return Surfaces[i];}

  static float distance2(Position p1, Position p2) {return (p1.x-p2.x)*(p1.x-p2.x) + (p1.y-p2.y)*(p1.y-p2.y) + (p1.z-p2.z)*(p1.z-p2.z);} 
  static bool TooClose(Position p1, Position p2, float min_threshold2=0, float max_y_threshold=FLT_MAX) {return ( distance2(p1, p2) <= min_threshold2) && (p1.y <= max_y_threshold) && (p2.y <= max_y_threshold);}
static float retX(Position p1) {return (p1.x);}
static float retY(Position p1) {return (p1.y);}

};


#endif

