#include "Matrix.h"
#include <stdio.h>
#include <iostream>


/*bool operator == (const Matrix& v1, const Matrix &v2){
  if(v1.GetSum()==v2.GetSum() && v1.GetCount() ==v2.GetCount() && v1.GetEuclidean() == v2.GetEuclidean()) {
    const int* mat1 = v1.GetMatrix();
    const int* mat2 = v2.GetMatrix();
    for(int i=0; i<SIZE*SIZE; ++i){
      if(mat1[i] != mat2[i])
        return false;
    }
    return true;
  }
  return false;
}

bool operator != (const Matrix& v1, const Matrix &v2){
  return !(v1==v2);
}*/





Matrix::Matrix(const std::array<int, SIZE*SIZE> mat_t, int type_t, int id_t){
  ID=id_t;
  euclidean=0;
  sum=0;
  count=0;
  type=type_t;
  //Mat = new int[SIZE*SIZE];
  for(int i=0; i<SIZE*SIZE; ++i){
    Mat[i]=mat_t[i];
    if(mat_t[i]!=0){
      sum+=mat_t[i];
      count+=1;
      int row=i/SIZE;
      int col=i%SIZE;
      euclidean += (row*row) + (col*col);
    }
  }
}
Matrix::Matrix(const Matrix & mat_t){
  ID= mat_t.ID;
  euclidean= mat_t.euclidean; 
  sum= mat_t.sum;
  type = mat_t.type;
  count= mat_t.count;
  //Mat = new int[SIZE*SIZE];
  for(int i=0;i<SIZE*SIZE; ++i)
    Mat[i]=mat_t.Mat[i];
}

Matrix & Matrix::operator=(const Matrix & mat_t){
  ID= mat_t.ID;
  type = mat_t.type;
  euclidean= mat_t.euclidean;
  sum= mat_t.sum;
  count= mat_t.count;
  //Mat = new int[SIZE*SIZE];
  for(int i=0;i<SIZE*SIZE; ++i)
    Mat[i]=mat_t.Mat[i];
  return *this;
}

/*Matrix::~Matrix(){
  delete Mat;
  }*/
int Matrix::GetType() const{
  return type;
}

int Matrix::GetSum() const
{
  return(sum);
}

int Matrix::GetCount() const
{
  return(count);
}

long Matrix::GetEuclidean() const
{
  return(euclidean);
}

int Matrix::GetID() const
{
  return(ID);
}
int Matrix::GetElement(int index) const
{
  return(Mat[index]);
}


/*int * Matrix::GetRow() //const
{
  return row;

  }*/
std::array<int, SIZE*SIZE> Matrix::GetMatrix() const
{
  return(Mat);
}

void Matrix::printMat() const{
  /*for(int i=0; i<SIZE; ++i){
    for(int j=0; j<SIZE; ++j){
    std::cout<<Mat[i*SIZE+j];
      if(j!=SIZE-1)
      std::cout<<", ";
      else
      std::cout<<"\n";
    }
    }*/
  for(int i=0; i< SIZE*SIZE; ++i){
    std::cout<<Mat[i];
    if(i%SIZE!=SIZE-1)
      std::cout<<", ";
    else
      std::cout<<std::endl;
  }
}



/*
string Matrix::to_string(){
  string matstr = ID + "^" + sum + "^" + count + "^" + euclidean + "^";
  }*/
