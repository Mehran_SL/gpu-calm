#include <iostream>
#include <Analysis.h>
#include <cstdlib>
#include <fstream>
#include <mpi.h>
#include <sstream>
#include <math.h>
#include <string>
#include <map>
#include <Matrix.h>
//#include <structs.h>
#include <set>
using namespace std;
//#define SIZE 367

vector<string> split(const string &s, char delim) {
  stringstream ss(s);
  string item;
  vector<string> tokens;
  while (getline(ss, item, delim)) {
    tokens.push_back(item);
  }
  return tokens;
}


string long2str(long i){
  stringstream ss;
  ss << i;
  return ss.str();
}

string int2str(int i){
  stringstream ss;
  ss << i;
  return ss.str();
}




template <typename T>
bool contains(const vector<T>& container, T element){
  typename vector<T>::const_iterator it;
  for(it = container.begin(); it!= container.end(); ++it){
    if(*it == element)
      return true;
  }
  return false;
}

string Matrices2Str(const vector<int>& uniques,const map<int, Matrix>& matrices, vector<string>& all_uniques){
  string result="", temp="";
  for(int i=0; i<uniques.size(); ++i){
    map<int, Matrix>::const_iterator tmp = matrices.find(uniques[i]);
    temp=(int2str(tmp->second.GetID())+"^"+int2str(tmp->second.GetSum())+"^"+int2str(tmp->second.GetCount())+"^"+long2str(tmp->second.GetEuclidean()) + "^");
    array<int, SIZE*SIZE> mat= tmp->second.GetMatrix();
    for(int j=0; j<SIZE; ++j){
      for(int k=0; k<SIZE; ++k){
	temp+=int2str(mat[j*SIZE+k]);
	if(k < SIZE-1)
	  temp += " ";
	else if(k==SIZE-1 and j<SIZE-1)
	  temp += "$";
      }
    }
    result+=temp;
    all_uniques.push_back(temp);
    if(i!=uniques.size()-1)
      result+="}";
  }
  return result;
}

string uniques2str(const vector<string>& uniques){
  string result="";
  int i=0;
  for(vector<string>::const_iterator it= uniques.begin(); it!=uniques.end(); ++it){
    result+=*it;
    if(i<uniques.size()-1)
      result+="}";
    i++;
  }
  return result;
}


void printMatrix(const std::array<int, SIZE*SIZE> matrix, ofstream & matrices_file ){
  for(int i=0; i<SIZE; ++i){
    for(int j=0; j<SIZE; ++j){
      matrices_file<<matrix[i*SIZE+j]<<" ";
    }
    matrices_file<<endl;
  }
}


vector<int> compareMatrices(const map<int, Matrix>& matrices, ofstream& output, ofstream& matrices_file, int Rank){
  map<int, Matrix>::const_iterator itr1, itr2;
  
  //vector<Matrix> distincts;
  vector<int> uniques;
  bool is_unique=false;
  int similar;
  if(matrices.size()>1){
    for(itr1=matrices.begin(); itr1!=matrices.end(); ++itr1){
      is_unique=false;
      for(itr2=matrices.begin(); itr2!=matrices.end(); ++itr2){
	if(itr1!=itr2){
	  if(itr1->second.GetCount()==itr2->second.GetCount() && itr1->second.GetSum()==itr2->second.GetSum() && itr1->second.GetEuclidean()==itr2->second.GetEuclidean()){
	    
	    array<int, SIZE*SIZE> m1 = itr1->second.GetMatrix();
            array<int, SIZE*SIZE> m2 = itr2->second.GetMatrix();


        for(int k=0; k<SIZE*SIZE; ++k){
          if(m1[k]!=m2[k]){
            is_unique=true;
            break;
          }
        }
	     

       if(!is_unique){
	 similar= itr2->second.GetID();
	 cout<<itr1->second.GetID()<<" = "<<itr2->second.GetID()<<" In "<<Rank<<endl;
	 if (Rank ==0){
	   matrices_file<<itr1->second.GetID()<<" = "<<itr2->second.GetID()<<endl;
	   printMatrix(m1, matrices_file);
	 }
       }
	     
	  }
	}
	
      }//End inner for
      if(is_unique||(!is_unique && !contains(uniques, similar))){
	uniques.push_back(itr1->second.GetID());
      }
    }//End outer for
  }//end if
  output<<"\t^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^\n"<<uniques.size()<<" Unique local matrices.\n";
  return uniques;
}



string printUniques(const vector<string>& uniques){
  string result="";
  for(vector<string>::const_iterator itr = uniques.begin(); itr!=uniques.end(); ++itr){
    result+=*itr;
    result+=" ";
  }
  result+="\n";
  return result;
}


void merge(vector<string>& uniques, string received, ofstream& output){
  vector<string> matrices = split(received,'}');
  bool is_unique;
  for(vector<string>::const_iterator itr=matrices.begin(); itr!= matrices.end(); ++itr){
    vector<string> features = split(*itr, '^');
    is_unique= true;
    for(vector<string>::const_iterator itr2=uniques.begin(); itr2!=uniques.end(); ++itr2){
      vector<string> features2 = split(*itr2, '^');
      //output<<features[1]<<"|"<<features2[1]<<"-"<<features[2]<<"|"<<features2[2]<<"-"<<features[3]<<"|"<<features2[3]<<endl;
      if(features[1]==features2[1] && features[2]==features2[2] && features[3]==features2[3] && features[4]==features2[4]){
	output<<features[0]<<" = "<<features2[0]<<"\n";
	is_unique=false;
	break;
      }else{
	//output<<"NOT SAME!\n";
      }
    }
    if(is_unique){
      uniques.push_back(*itr);
    }
  }
}
int main(int argc, char** argv){
  int Rank, NProcesses, Root=0, HumanCount;
  HumanCount=-1;
  MPI_Init(&argc, &argv);
  MPI_Comm_size(MPI_COMM_WORLD, &NProcesses);
  MPI_Comm_rank(MPI_COMM_WORLD, &Rank);
  //Ratio = NumberOfFiles/NProcesses+1;
  map< int, Matrix> Matrices;
  //vector<Matrix> Matrices;
  if(Rank==0){
    cout << NProcesses << "\n";
  }
  ofstream output, matrices_file;
  char ofile[128];
  
  sprintf(ofile, "Output%d", Rank);
  matrices_file.open("Similar_Matrices");
  output.open(ofile);
  
  Analysis::AnalysisParameters Param;
  if(!Analysis::ReadParameters(Param, Rank, NProcesses, "parameters"))
  {
   if(Rank==0)
     cerr  << "\nParameters should be in file: parameters and data should be in files: xmol.xyz_N\n";
   MPI_Finalize();
   exit(-1);
 }

 for(unsigned int i=0; i<Param.FileNames.size(); ++i)
 {
  Analysis analysis;
    //cout << "Analyzing File "<< index * 270 + i << endl;
  //output<<"Analyzing "<<Param.FileNames[i]<<"\n";
  if(!analysis.Initialize(Param.FileNames[i].data()))
  {
   cerr << "Unable to read data from file: " << Param.FileNames[i].data() << "\n";
   continue;
 }
  vector<string> names= split(Param.FileNames[i], '/');
  string name= names[names.size()-1];
 for(unsigned int j=0; j<Param.HumanThresholds.size(); ++j)
  for(unsigned int k=0; k<Param.SurfaceThresholds.size(); ++k)
  {
    //Analysis::ResultType result;
    float HumanThreshold, SurfaceThreshold;
    HumanThreshold = Param.HumanThresholds[j];
    SurfaceThreshold = Param.SurfaceThresholds[k];
    //int hcount = analysis.Series.GetHumanCount();
    //output<<"1- Calling Analyze() in "<<Rank<<" for "<<Param.FileNames[i]<<endl;
    
    Analysis::ResultType result = analysis.Analyze(HumanThreshold, SurfaceThreshold, Param.ExitThreshold, name, output);

    //if(Rank==NProcesses-1 && HumanCount==-1){
    //HumanCount=result.GetHumanCount();
    //}

    //output<<"Human Count: "<<HumanCount<<endl;
    Matrices.insert(make_pair<int, Matrix>(Rank+i*(NProcesses), Matrix(result.GetDirectInteractionCount(), 1, Rank+i*(NProcesses))));
    map<int,Matrix>::const_iterator this_matrix = Matrices.find((Rank+i*(NProcesses)));
    // output<<"ID: "<<this_matrix->second.GetID()<<", Sum: "<<this_matrix->second.GetSum()<<", Count: "<<this_matrix->second.GetCount()<<", Euclidean: "<<this_matrix->second.GetEuclidean()<<endl;
    output<<name<<":"<<this_matrix->second.GetSum()/2<<endl;
    // output<<"Matrix "<<Matrices.back().GetID()<<": Sum: "<<Matrices.back().GetSum()<<", Count: "<<Matrices.back().GetCount()<<", Euclidean: "<<Matrices.back().GetEuclidean()<<"\n";
      /*if (i< trunc(NumberOfFiles/NProcesses))
        mpialltransfer(NProcesses, Rank, Matrices.back().GetElement(0,0));*/

    } // End j: thresholds
  } // End i: filenames


 /*vector<string>all_unique_matrices;
 string matrices_str = Matrices2Str(compareMatrices(Matrices, output, matrices_file, Rank), Matrices, all_unique_matrices);
 //output<<matrices_str<<endl;
 //output<<"All unique matrices:\n"<<printUniques(all_unique_matrices);
 MPI_Status status;
 MPI_Request req;
 int step=0, index=Rank;
 bool continue_reduction=true;
 int received_size;
 while(continue_reduction){
   if(index%2==1){
     //Send
     MPI_Isend(matrices_str.c_str(), matrices_str.size(), MPI_CHAR, Rank - pow(2,step), 1, MPI_COMM_WORLD, &req);
     continue_reduction=false;
     MPI_Wait(&req, &status);
     //output<<"\nSent "<<matrices_str.c_str()<<" to "<<Rank-pow(2, step)<<" in step "<<step<<" with size = "<<matrices_str.size()<<endl;
   }else{
   MPI_Probe(Rank + pow(2, step), 1, MPI_COMM_WORLD, &status);
   MPI_Get_count(&status, MPI_CHAR, &received_size);
   //char * rec_buf = (char*)malloc(sizeof(char) * received_size + sizeof(char));
   char * rec_buf= new char[received_size+1](); 
   MPI_Irecv(rec_buf, received_size, MPI_CHAR, Rank+pow(2, step), 1, MPI_COMM_WORLD, &req);
   index/=2;
   MPI_Wait(&req, &status);
   string received_matrices(rec_buf);
   //output<<"\nReceived "<<received_matrices<<" from "<<Rank+pow(2, step)<<" in step "<<step<<" with size = "<<received_size<<" and this is the buffer: "<<rec_buf<<endl;
   delete [] rec_buf;
   merge(all_unique_matrices, received_matrices, output);
   output<<"------------------All unique matrices: "<<all_unique_matrices.size()<<" --------------------\n";//<<printUniques(all_unique_matrices)<<"\n --------------------------------------------------------------------------------\n";
   if(Rank==0){
     if(Rank+pow(2, step)<NProcesses/2)
       matrices_str=uniques2str(all_unique_matrices);
     else
       continue_reduction=false;
   }else
     matrices_str=uniques2str(all_unique_matrices);
   }//end receive
   step++;
   }//end While
 if(Rank==0){
   output<<"*****************************\n"<<"All unique matrices: "<<all_unique_matrices.size();//<<"\n********************************\n"<<printUniques(all_unique_matrices);
   }*/
 MPI_Finalize();
  return 0;
}


