#ifndef _ANALYSIS_H
#define _ANALYSIS_H

#include <string>
#include <Positions.h>
#include <PositionsSequence.h>
#include <vector>
#include <string>
#include <array>
#define SIZE 367

using namespace std;


class Analysis
{

  PositionsSequence Series;

 public:

  struct ResultStruct
  {
   private:
     //int Human, First_Human, Economy_Human, Surface, First_Surface, Economy_Surface, LastTimeStep;
     int LastTimeStep, HumanCount;
    //int *Direct_Interactions;
    //int *Indirect_Interactions;
    array<int, SIZE*SIZE> Direct_Interactions, Indirect_Interactions;
   public:
     /*int GetHumanInteractionCount() {return Human;}
     int GetFirstHumanInteractionCount() {return First_Human;}
     int GetEconomyHumanInteractionCount() {return Economy_Human;}
     int GetSurfaceInteractionCount() {return Surface;}
     int GetFirstSurfaceInteractionCount() {return First_Surface;}
     int GetEconomySurfaceInteractionCount() {return Economy_Surface;}
     int GetLastTimeStep() {return LastTimeStep;}*/

     int GetHumanCount() {return HumanCount;}
    array<int, SIZE*SIZE> GetDirectInteractionCount() {return Direct_Interactions;}
    array<int, SIZE*SIZE> GetIndirectInteractionCount() {return Indirect_Interactions;}
     int GetLastTimeStep() {return LastTimeStep;}
     //ResultStruct(int h=0, int fh=0, int eh=0, int s=0, int fs=0, int es=0, int l=0) : Human(h), First_Human(fh), Economy_Human(eh), Surface(s), First_Surface(fs), Economy_Surface(es), LastTimeStep(l) {}

    // ResultStruct(int *di, int *idi, int l=0, int n=0) : DirectInteractions(di), IndirectInteractions(idi), LastTimeStep(l), HumanCount(n) {}
  ResultStruct(array<int, SIZE*SIZE> di, array<int, SIZE*SIZE> idi, int l=0, int n=0):LastTimeStep(l), HumanCount(n){
    if(n<=0)
      n=1; 
      if(n>1){
	for (int r = 0; r <n*n; ++r){
	  Direct_Interactions[r] = di[r];
	  Indirect_Interactions[r] = idi[r];
	}
      }   
  }
    /*    ~ResultStruct(){
      delete Direct_Interactions;
      delete Indirect_Interactions;
      }*/
    ResultStruct& operator=(const ResultStruct& source){
      if(this != &source){
	int n=source.HumanCount;
	if(n<=0)
	  n=1;
	//Direct_Interactions = new int[n*n];
	//Indirect_Interactions = new int[n*n];
	if(n>1){
	  for (int r = 0; r <n*n; ++r){
	    Direct_Interactions[r] = source.Direct_Interactions[r];
	    Indirect_Interactions[r] = source.Indirect_Interactions[r];
	  }
	}
      }
      return *this;
    }
      ResultStruct& operator=(ResultStruct && source){
	Direct_Interactions=std::move(source.Direct_Interactions);
	Indirect_Interactions=std::move(source.Indirect_Interactions);
	return *this;
      }

	ResultStruct(const ResultStruct& source){
	  int n=source.HumanCount;
	  if(n<=0)
	    n=1;
	  //Direct_Interactions = new int[n*n];
	  //Indirect_Interactions = new int[n*n];
	  if(n>1){
	    for (int r = 0; r <n*n; ++r){
	      Direct_Interactions[r] = source.Direct_Interactions[r];
	      Indirect_Interactions[r] = source.Indirect_Interactions[r];
	    }
	  }
	}
    ResultStruct(ResultStruct && source){
      int n=source.HumanCount;
      if(n<=0)
	n=1;
      //Direct_Interactions = new int[n*n];
      //Indirect_Interactions = new int[n*n];
      Direct_Interactions=std::move(source.Direct_Interactions);
      Indirect_Interactions=std::move(source.Indirect_Interactions);
    }
      ResultStruct( int l=0, int n=0) : LastTimeStep(l), HumanCount(n) {
	if(n<=0)
	  n=1;
	//Direct_Interactions = new int[n*n];
	//Indirect_Interactions = new int[n*n];
      }
  };

  typedef ResultStruct ResultType;

  typedef struct
  {
     vector<float>  HumanThresholds;
     vector<float>  SurfaceThresholds;
     vector<string> FileNames;
     float          ExitThreshold;
  } AnalysisParameters;

  static bool ReadParameters(AnalysisParameters &Param, int Rank=0, int NProcs=1, const char *FileName="parameters");
  bool Initialize(const char *FileName) {return Series.ImportPositionsSequence(FileName);}
  ResultType Analyze(float human_threshold, float surface_threshold, float exit_threshold, string filename, ofstream& output);
};


#endif

