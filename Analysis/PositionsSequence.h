#ifndef _POSITIONSSEQUENCE_H
#define _POSITIONSSEQUENCE_H

#include <vector>
#include <Positions.h>

using namespace std;


class PositionsSequence
{

public:

  typedef vector<Positions> TimeSeries;

private:

  TimeSeries Data;
  int LastTimeStep;

public:

  bool ImportPositionsSequence(const char *FileName);
  int size(){return Data.size();}
  TimeSeries const & GetTimeSeries(){return Data;}
  int GetHumanCount(){return Data.size() ? Data[0].HumanSize() : 0;}
  int GetSurfaceCount(){return Data.size() ? Data[0].SurfaceSize() : 0;}
  Positions const & operator[](int i) {return Data[i];}
  int GetLastTimeStep() {return LastTimeStep;}
};


#endif

