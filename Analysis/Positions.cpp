#include <iostream>
#include <string>
#include <sstream>
#include <Positions.h>

using namespace std;


bool Positions::ImportPositions(int N, istream &input, int timestep)
{
  Position p_temp;

  for(int i=0; i<N; ++i)
  {
    string Line;
    getline(input, Line);
    stringstream in(Line);
    in >> p_temp.Type;
    in >> p_temp.x >> p_temp.y >> p_temp.z;
		p_temp.TimeStep=timestep;
    if(p_temp.Type == 'C' || p_temp.Type == 'O')
       Humans.push_back(p_temp);
    else
       Surfaces.push_back(p_temp);
  }

  return true;
}

