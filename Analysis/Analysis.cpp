#include <iostream>
#include <fstream>
#include <cstring>
#include <cstdio>
#include <Analysis.h>
#include <Positions.h>
#include <math.h>
#define obs 593
using namespace std;

bool Analysis::ReadParameters(AnalysisParameters &Param, int Rank, int NProcs, const char *FileName)
{
	ifstream InputFile(FileName);
	string s_temp;
	float minHumanThreshold, maxHumanThreshold, HumanThresholdIncr, minSurfaceThreshold, 
maxSurfaceThreshold, 
	SurfaceThresholdIncr;
	int FirstFileIndex, LastFileIndex, NumberOfFiles=8000;

	if(!InputFile)
		return false;

	InputFile>> s_temp;
	InputFile >> minHumanThreshold;
	InputFile>> s_temp;
	InputFile >> minSurfaceThreshold;
	InputFile>> s_temp;
	InputFile >> maxHumanThreshold;
	InputFile>> s_temp;
	InputFile >> maxSurfaceThreshold;
	InputFile>> s_temp;
	InputFile >> HumanThresholdIncr;
	InputFile>> s_temp;
	InputFile >> SurfaceThresholdIncr;
	InputFile>> s_temp;
	InputFile >> FirstFileIndex;
	InputFile>> s_temp;
	InputFile >> LastFileIndex;
	InputFile>> s_temp;
	InputFile >> Param.ExitThreshold;
	//	int Ratio = NumberOfFiles/NProcs+1;
	InputFile.close();
	FirstFileIndex=0;

	for(float f=minHumanThreshold; f<=maxHumanThreshold+HumanThresholdIncr/100.0; f+=HumanThresholdIncr)
		Param.HumanThresholds.push_back(f);

	for(float f=minSurfaceThreshold; f<=maxSurfaceThreshold+SurfaceThresholdIncr/100.0; 
f+=SurfaceThresholdIncr)
		Param.SurfaceThresholds.push_back(f);

	//for(int i=Rank; i<NumberOfFiles; i+=NProcs){
	int Ratio=NumberOfFiles/NProcs +1;
	for(int i=FirstFileIndex; (i<Ratio+FirstFileIndex and (Rank*Ratio)+i<NumberOfFiles+FirstFileIndex); ++i){
		char Name[128];
		sprintf(Name, "/pylon5/ci5618p/sadeghil/GPU/DRAM/Cutoff/Double/anim_%d", (Rank*Ratio)+i);
		Param.FileNames.push_back(string(Name));
	}

	return true;
}


Analysis::ResultType Analysis::Analyze(float human_threshold, float surface_threshold, float exit_threshold, 
				       string filename, ofstream &output)
{
	//int sum=0;
	const int N_Human = Series.GetHumanCount(), N_Surface = Series.GetSurfaceCount();
	
  /*int DirectInteractions[N_Human][N_Human];
  int IndirectInteractions[N_Human][N_Human];
  int SurfaceInteractions[N_Surface][N_Human];
  //long H_count = 0, FH_count = 0, EH_count = 0, S_count = 0, FS_count = 0, ES_count = 0, s_temp, fs_temp, 
es_temp;
 //int Direct_Interactions=0, Indirect_Interactions=0;
  memset(DirectInteractions, 0, N_Human*N_Human*sizeof(DirectInteractions[0][0]));
  memset(IndirectInteractions, 0, N_Human*N_Human*sizeof(IndirectInteractions[0][0]));
  memset(SurfaceInteractions, 0, N_Human*N_Surface*sizeof(SurfaceInteractions[0][0]));*/
	//int* IndirectInteractions = new int[N_Human*N_Human];
	//int* DirectInteractions = new int[N_Human*N_Human];
	array<int, SIZE*SIZE> DirectInteractions,IndirectInteractions;
	for (int r = 0; r < N_Human*N_Human; ++r){
		DirectInteractions[r] = 0;
		IndirectInteractions[r] = 0;
	}



	//	int* InteractionTimes = new int[N_Surface*N_Human];
	//int* SurfaceInteractions = new int[N_Surface*N_Human];
	array<int, SIZE*obs> InteractionTimes, SurfaceInteractions;
	for (int r = 0; r < N_Surface*N_Human; ++r){
		SurfaceInteractions[r] = 0;
		InteractionTimes[r] = 0;
	}
  //std::cout<<filename<<": Checkpoint 1 in Analyze!\n";

//////////////////////////////////////////////////////
	
	int sizeX,sizeY; sizeX=1800; sizeY=300;
	//int GridX,GridY; GridX=5; GridY=5;
	float coef= 0.02; //inch to meter 
	int lenghtNum=(int(sizeX*coef)+1);
	int widthNum=(int(sizeY*coef)+1);
	//vector <pair<int,int> > objects[lenghtNum][widthNum];
	//vector <pair<int,int> >  passengers[lenghtNum][widthNum];

// Griding the passengers in the plane
	vector <pair<int, int> > **passengers= new vector<pair<int, int> >*[lenghtNum];
	vector <pair<int, int> > **objects = new vector <pair<int, int> > *[lenghtNum];
	for(int i=0; i<lenghtNum; ++i){
	  passengers[i] = new vector<pair<int, int> >[widthNum];
	  objects[i] = new vector<pair<int, int> >[widthNum];
	}
	for(int i=0; i<lenghtNum; ++i){
	  for(int j=0; j<widthNum; ++j){
	    passengers[i][j] = vector<pair<int, int> >();
	    objects[i][j] = vector<pair<int, int> >();
	  }
	}
	float x1,y1;
	int indexX,indexY;	
	//Positions::Position p11;
	//cout << " TEST" << endl;
	for(int t=0; t<Series.size(); ++t)
	{
		for(int h=0; h<N_Human; ++h)
		{ 	
		//if (Positions::retX(Series[t].GetHuman(h))< x1)
			x1=Positions::retX(Series[t].GetHuman(h));
		//if (Positions::retY(Series[t].GetHuman(h))< y1) 
			y1=Positions::retY(Series[t].GetHuman(h));
			indexX=trunc((x1+20)*coef);
			indexY=trunc((y1+150)*coef);

			if(indexX>=0 && indexX<lenghtNum && indexY>=0 && indexY<widthNum){
			  //cout<<"1\n";
				passengers[indexX][indexY].push_back(make_pair(t,h));
				//cout<<"2\n";
			}/*else if(x1<1000){
		  //passengers[indexX][indexY].push_back(make_pair(t,h));
		  efile<<filename<<": x= "<<x1<<", y= "<<y1<<endl;
		  }*/

		}
	}



		human_threshold *= human_threshold;
		surface_threshold *= surface_threshold;

// Griding the objects in the plane
		
		for(int i=0; i<N_Surface; ++i){
			x1=Positions::retX(Series[0].GetSurface(i));
			y1=Positions::retY(Series[0].GetSurface(i));
			indexX=trunc((x1+20)*coef);
			indexY=trunc((y1+120)*coef);
			if(indexX>=0 && indexX<lenghtNum && indexY>=0 && indexY<widthNum){
			  objects[indexX][indexY].push_back(make_pair(0,i));
		
			}else{
			  cerr<<filename<<": x= "<<x1<<", y= "<<y1<<endl; 
			}
		}
		//std::cout<<filename<<": Checkpoint 2 in Analyze!\n";

		
// Direct impact comparison
		//vector <pair<int, int> > passengersGrid;
		//vector <pair<int, int> > objectsGrid;
		//vector <pair<int, int> > passengersAdjGrid;
		for(int x=0; x<lenghtNum; ++x){
			for(int y=0; y<widthNum; ++y){
				//passengersGrid=passengers[x][y];

				if (passengers[x][y].size()>0){

					vector <pair<int, int> >::iterator it1=passengers[x][y].begin();
					vector <pair<int, int> >::iterator it2=passengers[x][y].begin();
					for (it1=passengers[x][y].begin(); it1<passengers[x][y].end(); ++it1){
						for (it2=it1; it2<passengers[x][y].end(); it2++){
							if(it2!=it1 && it1->first==it2->first && it1->second!=it2->second ){								
								if(Positions::TooClose(Series[it1->first].GetHuman(it1->second), 
									Series[it2->first].GetHuman(it2->second),human_threshold, exit_threshold)){

									DirectInteractions[it2->second*N_Human+ it1->second]+=1;									
									DirectInteractions[it1->second*N_Human+ it2->second]+=1;
									//++sum;
									if(filename=="s_xmol.xyz_4"){
										output<<it1->first<<" , "<<it1->second<<" & "<<it2->first<<" , "<<it2->second<<" | "<<Positions::retX(Series[it1->first].GetHuman(it1->second))<<" , "<<Positions::retY(Series[it1->first].GetHuman(it1->second))<<" is too close to "<<Positions::retX(Series[it2->first].GetHuman(it2->second))<<" , "<<Positions::retY(Series[it2->first].GetHuman(it2->second))<<" So: "<<DirectInteractions[it2->second*N_Human+ it1->second]<<" & "<<DirectInteractions[it1->second*N_Human+ it2->second]<<std::endl;
									}	
								}
							}							
						}
						if (x+1< lenghtNum){ 
							vector <pair<int, int> >::iterator it2=passengers[x+1][y].begin();
							for (; it2!=passengers[x+1][y].end(); ++it2){
								if(it2!=it1 && it1->first==it2->first && it1->second!=it2->second){
									if(Positions::TooClose(Series[it1->first].GetHuman(it1->second), 
										Series[it2->first].GetHuman(it2->second),human_threshold, exit_threshold)){
										DirectInteractions[it2->second*N_Human+ it1->second]+=1;
									  	DirectInteractions[it1->second*N_Human+it2->second]+=1;
									  	//++sum;
									}
								}
							}
						}

						if (y+1< widthNum){ 
							vector <pair<int, int> >::iterator it2=passengers[x][y+1].begin();
							for (; it2!=passengers[x][y+1].end(); ++it2){
								if(it2!=it1 && it1->first==it2->first && it1->second!=it2->second && Positions::TooClose(Series[it1->first].GetHuman(it1->second), Series[it2->first].GetHuman(it2->second),human_threshold, exit_threshold)){
								  //if(filename=="s_xmol.xyz_4"){
								    	DirectInteractions[it2->second*N_Human+ it1->second]+=1;
								    	DirectInteractions[it1->second*N_Human+it2->second]+=1;
								    	//++sum;
									///	}
								}
							}
						}

						if ((y+1< widthNum) && (x+1<lenghtNum)){ 
							vector <pair<int, int> >::iterator it2=passengers[x+1][y+1].begin();
							for (; it2!=passengers[x+1][y+1].end(); ++it2){
								if(it2!=it1 && it1->first==it2->first && it1->second!=it2->second && Positions::TooClose(Series[it1->first].GetHuman(it1->second), Series[it2->first].GetHuman(it2->second),human_threshold, exit_threshold)){
								  	DirectInteractions[it2->second*N_Human+ it1->second]+=1;
								 	DirectInteractions[it1->second*N_Human+it2->second]+=1;
									//++sum;
			     				}
							}
						}

						if ((y-1 >= 0) && (x+1<lenghtNum)){ 
							vector <pair<int, int> >::iterator it2=passengers[x+1][y-1].begin();
							for (; it2!=passengers[x+1][y-1].end(); ++it2){
								if(it2!=it1 && it1->first==it2->first && it1->second!=it2->second && Positions::TooClose(Series[it1->first].GetHuman(it1->second), Series[it2->first].GetHuman(it2->second),human_threshold, exit_threshold)){
									DirectInteractions[it1->second*N_Human+it2->second]+=1;
									DirectInteractions[it2->second*N_Human+ it1->second]+=1;
									//++sum;
								}
							}
						}

						/*if (objects[x][y].size()>0){ 		
							vector <pair<int, int> >::iterator it2=objects[x][y].begin();
							for (; it2<objects[x][y].end(); ++it2){
								if(it2!=it1 && it1->first==it2->first && it1->second!=it2->second && Positions::TooClose(Series[it1->first].GetHuman(it1->second), Series[it2->first].GetSurface(it2->second), surface_threshold, exit_threshold)){
									SurfaceInteractions[it2->second*N_Human+it1->second] += 1;
									if(SurfaceInteractions[it2->second*N_Human+it1->second]==1)

									
					InteractionTimes[it2->second*N_Human+it1->second]=Series[it1->first].GetHuman(it1->second).TimeStep;
							}
						}
					}

if (x+1< lenghtNum){
	//objectsGrid=objects[x+1][y];
	if (objects[x+1][y].size()>0){ 		
		vector <pair<int, int> >::iterator it2=objects[x+1][y].begin();
		for (; it2<objects[x+1][y].end(); ++it2){

			if(it2!=it1 && it1->first==it2->first && it1->second!=it2->second && Positions::TooClose(Series[it1->first].GetHuman(it1->second), 
Series[it2->first].GetSurface(it2->second), 
				surface_threshold, exit_threshold)){

				SurfaceInteractions[it2->second*N_Human+it1->second] += 1;

			if(SurfaceInteractions[it2->second*N_Human+it1->second]==1)

				
InteractionTimes[it2->second*N_Human+it1->second]=Series[it1->first].GetHuman(it1->second).TimeStep;
		}
	}
}
}

if (y+1< widthNum){
	//objectsGrid=objects[x][y+1];
	if (objects[x][y+1].size()>0){ 		
		vector <pair<int, int> >::iterator it2=objects[x][y+1].begin();
		for (; it2<objects[x][y+1].end(); ++it2){

			if(it2!=it1 && it1->first==it2->first && it1->second!=it2->second && Positions::TooClose(Series[it1->first].GetHuman(it1->second), 
Series[it2->first].GetSurface(it2->second), 
				surface_threshold, exit_threshold)){

				SurfaceInteractions[it2->second*N_Human+it1->second] += 1;

			if(SurfaceInteractions[it2->second*N_Human+it1->second]==1)

				
InteractionTimes[it2->second*N_Human+it1->second]=Series[it1->first].GetHuman(it1->second).TimeStep;
		}
	}
}
}

if ((y+1< widthNum) && (x+1<lenghtNum)){
	//objectsGrid=objects[x+1][y+1];
	if (objects[x+1][y+1].size()>0){ 		
		vector <pair<int, int> >::iterator 
		it2=objects[x+1][y+1].begin();
		for (; it2<objects[x+1][y+1].end(); ++it2){

			if(it2!=it1 && it1->first==it2->first && it1->second!=it2->second && Positions::TooClose(Series[it1->first].GetHuman(it1->second), 
Series[it2->first].GetSurface(it2->second), 
				surface_threshold, exit_threshold)){

				SurfaceInteractions[it2->second*N_Human+it1->second] += 1;

			if(SurfaceInteractions[it2->second*N_Human+it1->second]==1)

				
InteractionTimes[it2->second*N_Human+it1->second]=Series[it1->first].GetHuman(it1->second).TimeStep;
		}
	}
}
}

if (y-1>= 0 && (x+1<lenghtNum)){
	//objectsGrid=objects[x+1][y-1];
	if (objects[x+1][y-1].size()>0){ 		
		vector <pair<int, int> >::iterator it2=objects[x+1][y-1].begin();
		for (; it2<objects[x+1][y-1].end(); ++it2){

			if(it2!=it1 && Positions::TooClose(Series[it1->first].GetHuman(it1->second), 
Series[it2->first].GetSurface(it2->second), 
				surface_threshold, exit_threshold)){

				SurfaceInteractions[it2->second*N_Human+it1->second] += 1;

			if(SurfaceInteractions[it2->second*N_Human+it1->second]==1)

				
InteractionTimes[it2->second*N_Human+it1->second]=Series[it1->first].GetHuman(it1->second).TimeStep;
		}
	}
}
}*/
				} //Passengers for loop
			} //If passengers Grid size
		} // y loop
	} //x loop



	
//////////////////////////////////////////////

// here should be modified

/*
 for(int t=0; t<Series.size(); ++t)
  {
    for(int h=0; h<N_Human; ++h)
      for(int i=0; i<h; ++i)
         if(Positions::TooClose(Series[t].GetHuman(h), Series[t].GetHuman(i), human_threshold, 
exit_threshold)){
              DirectInteractions[h][i]+=1;
         }


// here should be modified 
    for(int i=0; i<N_Surface; ++i){
      for(int h=0; h<N_Human; ++h){
        if(Positions::TooClose(Series[t].GetHuman(h), Series[t].GetSurface(i), surface_threshold, 
exit_threshold)){
          SurfaceInteractions[i][h] += 1;
          if(SurfaceInteractions[i][h]==1){
            InteractionTimes[i][h]=Series[t].GetHuman(h).TimeStep;
          }
	}
      }
    }
  }
*/
		//if(Series.size()==0){
	  //return ResultType(DirectInteractions, IndirectInteractions, 0, 0);
		//}
    //int contacts;
    /*
	for(int i=0; i<N_Surface; ++i){
    //contacts=0;
		for(int j=0; j<N_Human; ++j){
			if(SurfaceInteractions[i*N_Human+j]>0){
				for(int h=0; h<N_Human; ++h){
					if(SurfaceInteractions[i*N_Human+h]>0 && h!=j && 
						InteractionTimes[i*N_Human+j]>InteractionTimes[i*N_Human+h]){
            //IndirectInteractions[j][h]+=SurfaceInteractions[i][j];
						IndirectInteractions[j*N_Human+h]+=1;
				}
			}
		}
	}
}*/
  /*for(int h=H_count=FH_count=EH_count=0; h<N_Human; ++h)
    for(int i=0; i<h; ++i)
       if(HumanInteractions[h][i])
       {
         H_count++;
         if(Series[0].GetHumanClass(h) == 'O' && Series[0].GetHumanClass(i) == 'O')
           FH_count += 2;
         else if(Series[0].GetHumanClass(h) == 'C' && Series[0].GetHumanClass(i) == 'C')
           EH_count += 2;
         else
           {FH_count++; EH_count++;}
       }

  for(int i=S_count=FS_count=ES_count=0; i<N_Surface; ++i)
  {
    for(int h=s_temp=fs_temp=es_temp=0; h<N_Human; ++h)
      if(SurfaceInteractions[i][h])
      {
        s_temp++;
        if(Series[0].GetHumanClass(h) == 'O')
          fs_temp++;
        else
          es_temp++;
      }

    if(s_temp >= 2)
    {
      S_count += s_temp;
      FS_count += fs_temp;
      ES_count += es_temp;
    }
  }*/


	

	delete [] passengers;
	delete [] objects;
   ResultType res = ResultType(DirectInteractions, IndirectInteractions, Series.GetLastTimeStep(), N_Human);
   /* delete [] DirectInteractions;
   delete [] IndirectInteractions;
   delete [] SurfaceInteractions;
   delete [] InteractionTimes;
   
   if(filename=="s_xmol.xyz_4"){
     	std::cout<<"Object of Matrix 4:\n";
     	auto t = res.GetDirectInteractionCount();
     	for(int i=0; i<5; ++i){
       		for(int j=0; j<5; ++j){
	 			cout<<DirectInteractions[i*5+j]<<",";
       		}
       		cout<<endl;
     	}
   	}*/
   	//output<<filename<<":"<<sum<<endl;
   	return res;
  //std::cout<<"Number of Humans: "<<N_Human<<std::endl;

}




