# GPU CALM

This repository contains all the codes and instructions for running our CALM (Constrained Linear Movement) model. In what follows, we briefly explain the codes and guidelines for running them.


## GPU Parameter Sweep

These codes run a parameter sweep on the GPU. Two versions of code are available for using the shared memory or DRAM. Also, we have provided the single-precision (Float) and double-precision (Double) version of the code for each of those. To run the code, you should set the following parameters first:

- NumberOfBlocks should be set to the number of simulations (N) in "DataStructures.h" file
- For the Float versions, you can set the number of GPUs that you want to use at line 1305 of the "Simulation.cu" file. By default, the code will try to use all available devices.

Then you can compile the codes using the provided makefile. These makefiles will create an executable named "calm".

The output of the GPU simulations will be a single binary file named "anim". You can use the "decode.cpp" program to generate the text-file outputs. You need to set the number of simulations in this code and compile it using any C++ compiler. 


## Hybrid Parameter Sweep

This code can run a hybrid CPU-GPU parameter sweep by assigning a portion of the parameter space to the CPU and the rest to the GPU to sweep. For more details about the load-balancing techniques and technical explanation of different approaches, please refer to our paper: "GPU-Aware Pedestrian Dynamics Modeling".

To run the Block or Reverse Block versions of the Hybrid code, you can use the provided makefile in their directories to compile the codes. These makefiles will create an executable named "hybrid" that you can run with mpirun, for instance.

To run the Block-cyclic or Reverse block-cyclic versions of the hybrid implementation, you should first set the following parameters:
- The total number of simulations (N), CPU block size, GPU, block size, the total number of simulations on the CPU and the total number of simulations on the GPU should be set at line 31-35 of the "MyUtil.h" file. 
- NumberOfBlocks should be set to the number of GPU simulations in "DataStructures.h" and "GPUDataStructures.h".

Then you can compile the codes using the provided makefile. These makefiles will create an executable named "hybrid" that you can run with mpirun, for instance.

The output of the GPU simulations will be a single binary file named "anim". You can use the "decode.cpp" program to generate the text-file outputs. You just need to set the number of simulations in this code and compile it using any C++ compiler. 


## Interaction Analysis Code

This code can be found in the Analysis directory and will analyze the trajectory files to find the total number of contacts among passengers during the deplaning procedure. 

To run the code, you need to set the total number of trajectory files and directory of the trajectory files in lines 18 and 56 of the "Analysis.cpp" file.

Then you can use the provided makefile to compile the code. This makefile will create an executable file "Analyze" that you can run with mpirun, for instance.
