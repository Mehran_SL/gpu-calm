#include "MyUtil.h"



void split(const string &s, char delim, vector<string> &elems) {
  stringstream ss;
  ss.str(s);
  string item;
  while (getline(ss, item, delim)) {
    elems.push_back(item);
  }
}

double    microtime(void)
{
  struct timeval t;

  gettimeofday(&t, 0);

  return 1.0e6*t.tv_sec + (double) t.tv_usec;
}

double    get_microtime_resolution(void)
{
  double time1, time2;

  time1 = microtime();
  do
    {
      time2 = microtime();
    } while(time1==time2);

  return time2-time1;
}
