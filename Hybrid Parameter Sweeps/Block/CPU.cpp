/*
**************************************************************************
In principle, this file is same as movement.cpp, however, it is based on dynamic load
balancing with a next available core getting the next task.
Core 0 (Rank 0) acts as the master co-ordinating the work distribution

USAGE: takes two arguments (the number of parameter combinations)
mpirun -n 100 ./movement_lb1 1331 file
****(file - tasks id) - one column of tasks id ( to generate this file, order the tasks based on the time)
This file is NOT  the output of BalanceLoad !!!
**************************************************************************/
#include <iostream>
#include <fstream>
#include <unistd.h>
#include <string>
#include <vector>
#include <array>
#include <cmath>
#include <chrono>
#include <sstream>
#include <iterator>
#include <map>
#include <cstdlib>
#include <iomanip>
#include <pthread.h>
#include <time.h>
#include <stdio.h>
#include <mpi.h>
#include "Simulation.h"
#include "MyUtil.h"
#include "GPU.h"
void master(char *, int *);
void slave();
void CALM(Numeric sm, Numeric idt, Numeric isc, Numeric asc, Numeric tasc, Numeric rt, int ID);

using namespace std;

int id;    // process rank
int np;    // number of MPI processes
int NUM = 1700;   // number of tasks to be processed
char filename[32] = "sequences.txt";
int main ( int argc, char **argv );

void *KernelCaller(void *threadid) {

   cout << "Kernell is gonna be called now" << endl;
   Simulate();
   cout<<"Kernel RETURNED!\n";
   //pthread_exit(NULL);
}


//****************************************************************************80

int main ( int argc, char *argv[] )

//****************************************************************************80
{

  double time0 = microtime();
  int *SimIDs = new int[CPU_Sims];
  int index=0;
  for(int i=0; i<CPU_Sims; ++i){
        SimIDs[index++]=i;
  }
  /*int next_ID = ParameterSweepSize/(CPU_Step + GPU_Step) * (CPU_Step+GPU_Step) + (GPU_Sims - ParameterSweepSize/(CPU_Step + GPU_Step)*GPU_Step);
  while(index<CPU_Sims){
    SimIDs[index++]=next_ID++;
    }*/
  
  double time1;
  pthread_t thread;
  int r;

  MPI_Init (&argc, &argv);
  //cout<<"3\n";
  MPI_Comm_size(MPI_COMM_WORLD, &np);
  //cout<<"4\n";
  MPI_Comm_rank(MPI_COMM_WORLD, &id);
  //cout<<"5\n";
  // printf("Timer resolution = %g micro seconds\n", get_microtime_resolution());



  //NUM= atoi(argv[1]);
    if(id==0){
      r = pthread_create(&thread, NULL, KernelCaller, NULL);
      if(r){
	cout<<"Error in creatng thread!\n";
	exit(-1);
	}
    cout<<"NP: "<<np<<endl;
    cout<<"Num: "<<NUM<<endl;
     }


   if (id == 0) {
     time1= microtime();
            master(filename, SimIDs);
    } else {
            slave();
    }
   //  MPI_Finalize ( );

    if(id==0){
      double time2 = microtime();
      printf("CPU: Time taken = %g seconds\n", (time2-time1)/1.0e6);

     void* status;
    r = pthread_join(thread, &status);
    if (r) {
      cout << "Error Join" << r << endl;
      exit(-1);
    }
    //pthread_exit(NULL);
    }
    //cout<<id<<" Reached MPI_Finalize()\n";
    MPI_Finalize();
    delete[] SimIDs;
    if(id==0){
      double time3 = microtime();
      printf("Total Time = %g seconds\n", (time3-time0)/1.0e6);
    }
    return 0;
}



void master(char *filename, int* SimIDs)
{
  int     rank, work;
	float result[7];
	MPI_Status     status;
  ifstream aFile (filename);
  int index=0;
  string line;
	int p=0;

  for (rank = 1; rank < np; ++rank) {
    while(p!=SimIDs[index]){
      getline (aFile, line);
      p++;
    }
    index++;
    getline (aFile, line);
    vector<string> params;
		MPI_Request req;
		split(line, ' ', params);
		float parameters[7]; /*master will send the ID of next simulation along with six parameters in an array*/
		parameters[0] = p;
		//cout<<params.size()<<endl;
		for(int i=1; i<7; ++i){
		  //cout<<"i is "<<i<<endl;
		  parameters[i] = atof(params[i-1].c_str());
		}
                //work = nextp;      ; /* get_next_work_request */
		 MPI_Send(parameters,         /* message buffer */
                7,              /* one data item */
                MPI_FLOAT,        /* data item is an integer */
                rank,           /* destination process rank */
			  1,
			  MPI_COMM_WORLD);       /* user chosen message tag */
		 //cout<<p++<<" was assigned\n";
		 p++;
  }
  while ( p< ParameterSweepSize && index<CPU_Sims)
  {
    while(p!=SimIDs[index]){
      getline (aFile, line);
      p++;
    }
    index++;
    std::getline (aFile, line);
    vector<string> params;
    split(line, ' ', params);
    float parameters[7]; /*master will send the ID of next simulation along wi\
   th six parameters in an array*/
    parameters[0] = p;
    for(int i=1; i<7; ++i){
      parameters[i] = atof(params[i-1].c_str());
    }
    MPI_Recv(&result,       /* message buffer */
    1,              /* one data item */
    MPI_INT,     /* of type double real */
    MPI_ANY_SOURCE, /* receive from any sender */
    MPI_ANY_TAG,    /* any type of message */
		MPI_COMM_WORLD,
                &status);       /* received message info */
		MPI_Request req;
		MPI_Send(parameters, 7, MPI_FLOAT, status.MPI_SOURCE,
			 1, MPI_COMM_WORLD);
		//cout<<p++<<" was assigned\n";
		p++;
    }
    for (rank = 1; rank < np; ++rank) {
      MPI_Recv(&result, 7, MPI_FLOAT, MPI_ANY_SOURCE,
		  MPI_ANY_TAG, MPI_COMM_WORLD, &status);
      float fake_parameters[7];

	    for(int i=0; i<7; ++i){
	       fake_parameters[i] = -1;
	    }
	    MPI_Send(fake_parameters, 7, MPI_FLOAT, status.MPI_SOURCE, 2, MPI_COMM_WORLD);
    }
}



void slave()
{
  int              result;
  float                 work[7];
  MPI_Status          status;
  char                buffer[200];
  for (;;) {
	  MPI_Recv(&work, 7, MPI_FLOAT, 0, MPI_ANY_TAG, MPI_COMM_WORLD, &status);
		MPI_Request req;
    if (status.MPI_TAG == 2) {
      return;
    }
		CALM(work[1], work[2], work[3], work[4], work[5], work[6], work[0]);
		result = 1; /* do the work */
    MPI_Send(&result, 1, MPI::INT, 0, 0, MPI_COMM_WORLD);
  }
}


//*****************************************************************************

void CALM(Numeric sm, Numeric idt, Numeric isc, Numeric asc, Numeric tasc, Numeric rt, int ID){
  Numeric min_SM = 1.1f;
  Numeric min_IDT = 0.2f;
  Numeric min_ISC = 0.2f;
  Numeric min_ASC = 0.2f;
  Numeric min_TASC = 0.2f;
  Numeric min_RT = 0.5f;

  Numeric max_SM = 1.3f;
  Numeric max_IDT = 1.5f;
  Numeric max_ISC = 0.8f;
  Numeric max_ASC = 0.7f;
  Numeric max_TASC = 0.6f;
  Numeric max_RT = 1.6f;

  Numeric this_SM = (max_SM - min_SM) * sm + min_SM;
  Numeric this_IDT = (max_IDT - min_IDT) * idt + min_IDT;
  Numeric this_ISC = (max_ISC - min_ISC) * isc + min_ISC;
  Numeric this_ASC = (max_ASC - min_ASC) * asc + min_ASC;
  Numeric this_TASC = (max_TASC - min_TASC) * tasc + min_TASC;
  Numeric this_RT = (max_RT - min_RT) * rt + min_RT;

  ofstream output;
  char ofile[128];
  sprintf(ofile, "output%d", ID);
  output.open(ofile);

  output<<"Running simulation with SimID= "<<ID<<", IDT= "<<this_IDT<<", ISC= "<<this_ISC<<", ASC= "<<this_ASC<<", TASC= "<<this_TASC<<", RT= "<<this_RT<<", SM= "<<this_SM<<endl;
  Simulation sim = Simulation(this_SM, 0.2f, this_IDT, this_ISC, this_ASC, this_TASC, this_RT, ID);

  int status = sim.Run(output, ID);
  if(status!=0)
    cout<<"Err in: "<<ID<<endl;

}
