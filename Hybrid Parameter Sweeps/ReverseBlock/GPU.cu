#include <iostream>
#include <cuda.h>
#include <curand_kernel.h>
#include <vector>
#include <stdio.h>
#include <thrust/host_vector.h>
#include <thrust/device_vector.h>
#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <sstream>
#include <iterator>
#include <map>
#include <iomanip>
#include <random>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include "GPUDataStructures.h"
#include "MyUtil.h"
#include "GPU.h"


using namespace std;

#include <sys/time.h>

__device__ uint get_smid(void) {

	uint ret;

	asm("mov.u32 %0, %smid;" : "=r"(ret) );

	return ret;

}


void CheckCUDAError(const char *msg) /* From nvidia material */
{
	cudaError_t err = cudaGetLastError();

	if(cudaSuccess != err)
	{
		fprintf(stderr, "CUDA error: %s: %s.\n", cudaGetErrorString(err));
		exit(2);
	}
}



void allocateMem(Passengers &passengers, Obstacles& obstacles){

	if(cudaMallocManaged((void**)&(passengers.priority), NumberOfPassengers * NumberOfBlocks * sizeof(Numeric)) != cudaSuccess)
		printf("Mem Aloc Error 2\n");
	if(cudaMallocManaged((void**)&(passengers.waitTime), NumberOfPassengers * NumberOfBlocks * sizeof(int)) != cudaSuccess)
		printf("Mem Aloc Error 3\n");
	if(cudaMallocManaged((void**)&(passengers.aim), NumberOfPassengers * NumberOfBlocks * sizeof(Aim)) != cudaSuccess)
		printf("Mem Aloc Error 5\n");
	if(cudaMallocManaged((void**)&(passengers.position_x), NumberOfPassengers * NumberOfBlocks * sizeof(Numeric)) != cudaSuccess)
		printf("Mem Aloc Error 6\n");
	if(cudaMallocManaged((void**)&(passengers.position_y), NumberOfPassengers * NumberOfBlocks * sizeof(Numeric)) != cudaSuccess)
		printf("Mem Aloc Error 7\n");
	if(cudaMallocManaged((void**)&(passengers.velocity_x), NumberOfPassengers * NumberOfBlocks * sizeof(Numeric)) != cudaSuccess)
		printf("Mem Aloc Error 8\n");
	if(cudaMallocManaged((void**)&(passengers.velocity_y), NumberOfPassengers * NumberOfBlocks * sizeof(Numeric)) != cudaSuccess)
		printf("Mem Aloc Error 9\n");
	if(cudaMallocManaged((void**)&(passengers.seat_position_x), NumberOfPassengers * NumberOfBlocks * sizeof(Numeric)) != cudaSuccess)
		printf("Mem Aloc Error 10\n");
	if(cudaMallocManaged((void**)&(passengers.has_released), NumberOfPassengers * NumberOfBlocks * sizeof(bool)) != cudaSuccess)
		printf("Mem Aloc Error 12\n");
	if(cudaMallocManaged((void**)&(passengers.desiredSpeed), NumberOfPassengers * NumberOfBlocks * sizeof(Numeric)) != cudaSuccess)
		printf("Mem Aloc Error 13\n");
	if(cudaMallocManaged((void**)&(passengers.ID), NumberOfPassengers * NumberOfBlocks * sizeof( int)) != cudaSuccess)
		printf("Mem Aloc Error 14\n");
	if(cudaMallocManaged((void**)&(passengers.race_position_x), NumberOfPassengers * NumberOfBlocks * sizeof(Numeric)) != cudaSuccess)
		printf("Mem Aloc Error 16\n");
	if(cudaMallocManaged((void**)&(passengers.race_position_y), NumberOfPassengers * NumberOfBlocks * sizeof(Numeric)) != cudaSuccess)
		printf("Mem Aloc Error 17\n");
	if(cudaMallocManaged((void**)&(passengers.race_opID), NumberOfPassengers * NumberOfBlocks * sizeof( int)) != cudaSuccess)
		printf("Mem Aloc Error 20\n");
	if(cudaMallocManaged((void**)&(passengers.race_is_finished), NumberOfPassengers * NumberOfBlocks * sizeof( int)) != cudaSuccess)
		printf("Mem Aloc Error 21\n");
	if(cudaMallocManaged((void**)&(passengers.race_counter), NumberOfPassengers * NumberOfBlocks * sizeof(int)) != cudaSuccess)
		printf("Mem Aloc Error 22\n");
	if(cudaMallocManaged((void**)&(passengers.race_status), NumberOfPassengers * NumberOfBlocks * sizeof(int)) != cudaSuccess)
		printf("Mem Aloc Error 23\n");
	if(cudaMallocManaged((void**)&(passengers.force_x), NumberOfPassengers * NumberOfBlocks * sizeof(Numeric)) != cudaSuccess)
		printf("Mem Aloc Error 24\n");
	if(cudaMallocManaged((void**)&(passengers.force_y), NumberOfPassengers * NumberOfBlocks * sizeof(Numeric)) != cudaSuccess)
		printf("Mem Aloc Error 25\n");
	if(cudaMallocManaged((void**)&(passengers.nearest_ID), NumberOfPassengers * NumberOfBlocks * sizeof( int)) != cudaSuccess)
		printf("Mem Aloc Error 26\n");
	if(cudaMallocManaged((void**)&(passengers.nearest_position_x), NumberOfPassengers * NumberOfBlocks * sizeof(Numeric)) != cudaSuccess)
		printf("Mem Aloc Error 27\n");
	if(cudaMallocManaged((void**)&(passengers.nearest_position_y), NumberOfPassengers * NumberOfBlocks * sizeof(Numeric)) != cudaSuccess)
		printf("Mem Aloc Error 28\n");
	if(cudaMallocManaged((void**)&(passengers.nearest_distance), NumberOfPassengers * NumberOfBlocks * sizeof(Numeric)) != cudaSuccess)
		printf("Mem Aloc Error 29\n");
	if(cudaMallocManaged((void**)&(obstacles.positions_x), NumberOfObstacles * sizeof(Numeric)) != cudaSuccess)
		printf("Mem Aloc Error 30\n");
	if(cudaMallocManaged((void**)&(obstacles.positions_y), NumberOfObstacles * sizeof(Numeric)) != cudaSuccess)
		printf("Mem Aloc Error 31\n");
}

void freeMem(Passengers &passengers, Obstacles& obstacles, Numeric* d_outputs){


	cudaFree(passengers.priority);
	cudaFree(passengers.waitTime);
	cudaFree(passengers.aim);
	cudaFree(passengers.position_x);
	cudaFree(passengers.position_y);
	cudaFree(passengers.velocity_x);
	cudaFree(passengers.velocity_y);
	cudaFree(passengers.seat_position_x);
	cudaFree(passengers.has_released);
	cudaFree(passengers.desiredSpeed);
	cudaFree(passengers.ID);
	cudaFree(passengers.race_position_x);
	cudaFree(passengers.race_position_y);
	cudaFree(passengers.race_opID);
	cudaFree(passengers.race_is_finished);
	cudaFree(passengers.race_counter);
	cudaFree(passengers.race_status);
	cudaFree(passengers.force_x);
	cudaFree(passengers.force_y);
	cudaFree(passengers.nearest_ID);
	cudaFree(passengers.nearest_position_x);
	cudaFree(passengers.nearest_position_y);
	cudaFree(passengers.nearest_distance);
	cudaFree(obstacles.positions_x);
	cudaFree(obstacles.positions_y);
	cudaFree(d_outputs);

}


/*void split(const string &s, char delim, vector<string> &elems) {
	stringstream ss;
	ss.str(s);
	string item;
	while (getline(ss, item, delim)) {
		elems.push_back(item);
	}
}*/

/*
This function reads the input files and then creates, initializes and stores the data in appropriate datastructures.
*/
void Initialize(Passengers &passengers_t, Obstacles &obstacles_t, char*__restrict xyz_obs, vector<Numeric>& sim_params, vector<pair<int, int> >& arrangement){
	/*Numeric mean=1.15f;
	Numeric stddev= 0.2f;
	std::default_random_engine gen;
	std::normal_distribution<Numeric> ND(mean, stddev);*/

	Numeric tmpPosition_x, tmpPosition_y;
	int index=0, obs_index=0;
	char coord_file[128];
	sprintf(coord_file, "coord_A320_144.d_coordinates.txt");
	ifstream coords (coord_file);
	string line;

	multimap<Numeric, Numeric> positions_map;
	vector<string> tmp;
	stringstream buffer_stream;
	//cout<<"1\n";
	if (coords.is_open()){
		buffer_stream << coords.rdbuf();
		coords.close();
	}else{
		cout<<"Error in reading coordinates file!\n";
	}

	while (getline (buffer_stream,line) ){
		if(index<=1){
			split(line, '|', tmp);
			for(int i=0; i<int(tmp.size());i++){

				if (tmp[i].find('@')!=std::string::npos){
					vector<string> pair;
					split(tmp[i], '@', pair);
					arrangement.push_back(make_pair(atoi(pair[0].c_str()), atoi(pair[1].c_str())));
				}else{
					sim_params.push_back((Numeric)atof(tmp[i].c_str()));
				}

			}
			index++;
		}else{
			split(line, ' ', tmp);
			tmpPosition_x=(Numeric)(atof(tmp[1].c_str()));
			tmpPosition_y= (Numeric)(atof(tmp[2].c_str()));
			if(tmp[0]=="13"){
				positions_map.insert(make_pair(tmpPosition_x, tmpPosition_y));
			}else if(tmp[0]=="16"){
				obstacles_t.positions_x[obs_index]=tmpPosition_x;
				obstacles_t.positions_y[obs_index++]=tmpPosition_y;
			}
			index++;
		}
		tmp.clear();
	}
		//cout<<"2\n";
	char xyz[128];
	sprintf(xyz, "xyz_obs_1");
	stringstream xyz_buff_stream;
	ifstream xyz_file(xyz);
	if(xyz_file.is_open()){
		xyz_buff_stream << xyz_file.rdbuf();
		xyz_file.close();
	}else{
		cout<<"Error in openning xyz_obstacles.txt!\n";
	}
	string xyz_line;
	while(getline(xyz_buff_stream, xyz_line)){
		strcat(xyz_obs, xyz_line.c_str());
		strcat(xyz_obs, "\n");
	}
	xyz_obs[strlen(xyz_obs)-1]=0;

	/*initializing the passengers, determining the initial aim and the priority of passengers*/
	Numeric row=positions_map.begin()->first;
	Numeric tmpPriority=-1;
	//Numeric priority;
	Numeric position_x;
	Numeric position_y;
	int ID=0;
	for(multimap<Numeric,Numeric>::iterator itr=positions_map.begin(); itr!=positions_map.end(); ++itr){
		Aim initial_aim;
		if(itr->second > 0){
			initial_aim=DOWN_TO_AISLE;
		}else{
			initial_aim=UP_TO_AISLE;
		}
		position_x=Numeric(itr->first);
		position_y=Numeric(itr->second);
		if(itr->first != row){
			row = itr->first;
			tmpPriority--;
		}
		//priority = tmpPriority;
		for(int pp=0; pp<NumberOfBlocks; ++pp){
			//passengers_t.mass[pp*NumberOfPassengers + ID] = (Numeric)1.0f;
			passengers_t.priority[pp*NumberOfPassengers +ID] = tmpPriority;
			passengers_t.waitTime[pp*NumberOfPassengers +ID] = 0.0f;
			passengers_t.aim[pp*NumberOfPassengers +ID] = initial_aim;
			passengers_t.position_x[pp*NumberOfPassengers +ID] = position_x;
			passengers_t.position_y[pp*NumberOfPassengers +ID] = position_y;
			passengers_t.velocity_x[pp*NumberOfPassengers +ID] = 0.0f;
			passengers_t.velocity_y[pp*NumberOfPassengers +ID] = 0.0f;
			passengers_t.seat_position_x[pp*NumberOfPassengers +ID] = position_x;
			passengers_t.has_released[pp*NumberOfPassengers +ID] = false;
			passengers_t.ID[pp*NumberOfPassengers +ID] = ID;
			passengers_t.race_position_x[pp*NumberOfPassengers +ID] = (Numeric)-100.0f;
			passengers_t.race_position_y[pp*NumberOfPassengers +ID] = (Numeric)-100.0f;
			passengers_t.race_opID[pp*NumberOfPassengers +ID] = -10;
			passengers_t.race_is_finished[pp*NumberOfPassengers +ID] = 1; //true=1 false=0
			passengers_t.race_counter[pp*NumberOfPassengers +ID] = 0;
			passengers_t.race_status[pp*NumberOfPassengers +ID] = NO_RACE;
			passengers_t.force_x[pp*NumberOfPassengers +ID] = Numeric(0.0f);
			passengers_t.force_y[pp*NumberOfPassengers +ID] = Numeric(0.0f);
			passengers_t.nearest_ID[pp*NumberOfPassengers +ID] = -10;
			passengers_t.nearest_position_x[pp*NumberOfPassengers +ID] = (Numeric)-100.0f;
			passengers_t.nearest_position_y[pp*NumberOfPassengers +ID] = (Numeric)-100.0f;
			passengers_t.nearest_distance[pp*NumberOfPassengers +ID] = (Numeric)100.0f;
			/*Numeric speed = ND(gen);
			while(speed<0.7 || speed>1.6){
				speed = ND(gen);
			}*/
			passengers_t.desiredSpeed[pp*NumberOfPassengers +ID] = 0.0f;// * 10.0f;

		}
		ID++;

	}
}

/*
This function is used to initialize the aisle permissions so that we can make sure that passengers from
back rows, will not exit before passengers from front rows.
*/
void InitializePermissions(int*__restrict holds, bool*__restrict aisle_permission, const vector<pair<int, int> >& arrangement){
	int Total_Number_Of_Rows=0, index=0;
	for (int i = arrangement.size()-1; i>=0 ; --i){
		Total_Number_Of_Rows+=arrangement[i].first;
		for(int j=0; j<arrangement[i].first; ++j){
			holds[index] = arrangement[i].second;
			aisle_permission[index++] = false;
		}
	}
	aisle_permission[Total_Number_Of_Rows-1]=true;

	for(int i=1; i<NumberOfBlocks; ++i){
		for(int j=0; j<Total_Number_Of_Rows; ++j){
			holds[i*Total_Number_Of_Rows+j] = holds[j];
			aisle_permission[i*Total_Number_Of_Rows+j] = aisle_permission[j];
		}
	}
}


/*
Getting the separations distance between two passengers.
Separation distance is the distance between two passengers
in the direction of movement of the main passenger.
And main passenger is the one we are going to find the nearest passenger to.
*/

__device__ Numeric getSeparation(const Numeric&__restrict main_position_x, const Numeric&__restrict main_position_y, const Aim&__restrict main_aim_t, const Numeric&__restrict sec_position_x, const Numeric&__restrict sec_position_y, const Aim&__restrict sec_aim_t){
	bool condition1, condition2, condition3, condition4, condition5;
	condition1 = (main_aim_t == DOWN_TO_AISLE && fabs(sec_position_x - main_position_x)<(Numeric)0.1f);
	condition2 = ((main_aim_t == UP_TO_AISLE) && fabs(sec_position_x - main_position_x)<(Numeric)0.1f);
	condition3 = (( main_aim_t==sec_aim_t && main_aim_t == TOWARD_EXIT) && fabs(sec_position_x - main_position_x)<(Numeric)0.15f);
	condition4 = (main_aim_t==IN_AISLE && fabs(sec_position_y - main_position_y)<(Numeric)0.15f && (sec_aim_t==ALIGNING || sec_aim_t==IN_AISLE));
	condition5 = ((sec_position_x - main_position_x > (Numeric)-0.04f && sec_position_x - main_position_x<(Numeric)0.15f));
	if(condition1){
		return main_position_y - sec_position_y;
	}else if(condition2){
		return sec_position_y - main_position_y;
	}else if(condition3){
		return sec_position_y - main_position_y;
	}else if (condition4){
		return sec_position_x - main_position_x;
	}else if(main_aim_t==ALIGNING){
		if(sec_position_x<main_position_x){
			return (Numeric)0.75f;
		}
		else if(condition5){
			if(main_position_y>(Numeric)0.06f){
				return main_position_y - sec_position_y;
			}else if((main_position_y<-(Numeric)0.06f)){
				return sec_position_y - main_position_y;
			}
		}
	}
	return (Numeric)100.0f;
}




/*
This function is called to find the nearest passenger
to the main passenger in his/her direction of movement.
And main passenger is the one we are going to find the nearest passenger to.
*/

__device__ void getNearestPassenger(const int&__restrict passenger_ID, const Numeric&__restrict exit_position, Aim*__restrict aim, Numeric*__restrict position_x, Numeric*__restrict position_y, int&__restrict nearest_ID, Numeric&__restrict nearest_position_x, Numeric&__restrict nearest_position_y, Numeric&__restrict nearest_distance){
	bool condition1, condition2;
	nearest_distance = 100.0f;
	nearest_ID = -10;
	for(int i=0; i<NumberOfPassengers; ++i){
		condition1 = (aim[passenger_ID] != OUT && i != passenger_ID);
		if(condition1){
			Numeric separation = getSeparation(position_x[passenger_ID], position_y[passenger_ID], aim[passenger_ID], position_x[i], position_y[i], aim[i]);
			condition2 = (separation<nearest_distance && separation>0);
			if (condition2){
				nearest_distance = separation;
				nearest_position_x = position_x[i];
				nearest_position_y = position_y[i];
				nearest_ID = i;
			}
		}//END if initial conditions
	}//End for
	//printf("getNearestPassenger %d √\n", id);

}





/*
Getting the separations distance between a passenger and a physical obstacle in plane.
Separation distance is the distance between the passenger and obstacle
in the direction of movement of the passenger.
*/
__device__ Numeric getObsSeparation(const Numeric &__restrict main_position_x, const Numeric &__restrict main_position_y, const Aim&__restrict main_aim_t, const Numeric &__restrict obs_position_x, const Numeric &__restrict obs_position_y){
	bool condition1, condition2, condition3, condition4;
	condition1 = (main_aim_t == 0 && fabs(obs_position_x - main_position_x)<(Numeric)0.05f);
	condition2 = ((main_aim_t == 1) && fabs(obs_position_x - main_position_x)<(Numeric)0.05f);
	condition3 = ((main_aim_t == 4) && fabs(obs_position_x - main_position_x)<(Numeric)0.05f);
	condition4 = (main_aim_t==3 && fabs(obs_position_y - main_position_y)<(Numeric)0.05f);
	if(condition1){
		return main_position_y - obs_position_y;
	}else if(condition2){
		return obs_position_y - main_position_y;
	}else if(condition3){
		return obs_position_y - main_position_y;
	}else if (condition4){
		return obs_position_x - main_position_x;
	}

	return (Numeric)10.0f;
}




/*
This function checks if the path of a passenger is blocked by a physical obstacle.
*/
__device__ bool blockedPath(const int&__restrict passenger_ID, Numeric*__restrict obstacle_positions_x, Numeric*__restrict obstacle_positions_y, const Numeric&__restrict passenger_position_x, const Numeric&__restrict passenger_position_y, const Aim&__restrict passenger_aim){
	bool condition, flag=false;
	for(int i=0; i<NumberOfObstacles; ++i){

		Numeric separation= getObsSeparation(passenger_position_x, passenger_position_y, passenger_aim, obstacle_positions_x[i], obstacle_positions_y[i]);
		condition = (separation<Numeric(0.04f) && separation>Numeric(0.0f));
		if(condition){
			flag = true;
		}
	}

	return flag;
}

/*
This function decreases the desired speed of the passenger regarding
his/her distance to the nearest passenger is his/her direction of movement.

__device__ Numeric DecreaseSpeed(const Numeric&__restrict desired_speed_t, const Numeric&__restrict stop_threshold, const Numeric&__restrict distance){

	return desired_speed_t * ((Numeric)1.0f - (Numeric) ((stop_threshold)/distance));
}*/

	__device__ Numeric DecreaseSpeed(const Numeric&__restrict coeff, const Numeric&__restrict distance, const Numeric&__restrict desired_speed){

		Numeric a=2.111f, b=0.366f, c=0.966f;
		return coeff*Numeric(c-exp(-a * (distance-b))) * desired_speed;
	}


/*
This function is used to check the distance with a passenger behind of
main passenger (caller of function) when the main passenger is moving backward
(opposite of his/her desired direction)
*/
__device__ void checkBehind( int* IDs, const  int& ID, Numeric*__restrict position_x, Numeric*__restrict position_y, Numeric&__restrict velocity_x, Numeric&__restrict velocity_y, Numeric&__restrict force_x, Numeric&__restrict force_y){
	bool condition;
	for(int n=0; n<NumberOfPassengers; ++n){
		condition = ( IDs[n] != ID && fabs(position_y[IDs[n]]-position_y[ID])<0.1f && position_x[ID]<= position_x[IDs[n]]+ 0.15f);
		if (condition){
			force_x=0.0f;
			force_y=0.0f;
			velocity_x=0.0f;
			velocity_y=0.0f;
		}
	}
}


/*
We use this function to update velocity and position of passengers based on their received forces.
*/
__device__ void EulerMethod(const int&__restrict race_status, const Numeric&__restrict force_x, const Numeric&__restrict force_y, Numeric&__restrict velocity_x, Numeric&__restrict velocity_y, Numeric&__restrict position_x, Numeric&__restrict position_y, const Aim&__restrict aim, const Numeric&__restrict seat_position_x, const Numeric&__restrict desired_speed, const Numeric&__restrict delta){
	Numeric Speed, new_velocity_x, new_velocity_y, new_position_x, new_position_y;
	/*updating the velocity using Euler method*/
	new_velocity_x=(force_x * (Numeric)(delta) + velocity_x);
	new_velocity_y=(force_y * (Numeric)(delta) + velocity_y);
	bool condition = race_status==WINNER && new_velocity_x<0;
	if(condition){
		velocity_x = velocity_x * 0.1f;
		velocity_y = velocity_y * 0.1f;
		position_x = (velocity_x * delta) + position_x;
		position_y = (velocity_y * delta) + position_y;
	}else{
		velocity_x = new_velocity_x;
		velocity_y = new_velocity_y;

		Speed = (velocity_x * velocity_x) + (velocity_y * velocity_y);
		/*updating the position using Euler method*/

		if(Speed<=(desired_speed*desired_speed)){
			new_position_x = (velocity_x * delta) + position_x;
			new_position_y = (velocity_y * delta) + position_y;
		}else{
			new_position_x = (velocity_x * (desired_speed/(Speed+0.01)) * delta) + position_x;
			new_position_y = (velocity_y * (desired_speed/(Speed+0.01)) * delta) + position_y;
		}
		position_x = new_position_x;
		position_y = new_position_y;
	}
}


/*
This function computes the propulsion of passenger for moving toward the exit door
with his/her maximum desired speed
*/
__device__ void Propulsive_Force(const Aim &__restrict aim, const Numeric&__restrict position_y, const Numeric&__restrict velocity_x, const Numeric&__restrict velocity_y, const Numeric&__restrict time_step, const Numeric&__restrict desired_speed, Numeric&__restrict force_x, Numeric&__restrict force_y){
	Numeric e1_x=0.0f, e1_y=0.0f, new_velocity_x, new_velocity_y;
	if(aim==DOWN_TO_AISLE){
		e1_y = -1;
	}else if(aim==UP_TO_AISLE){
		e1_y = 1;
	}else if(aim==ALIGNING){
		if(position_y>(Numeric)0.06f){
			e1_x=0.5f;
			e1_y=-0.9f; //0.9~sqrt(3)/2
		}else if(position_y<-(Numeric)0.06f){
			e1_x= 0.5f;
			e1_y= 0.9f;
		}
	}else if(aim==IN_AISLE){
		/********ALIGNMENT********/
		/* Passengers should move on almost a line in the center of aisle*/
		if(position_y>(Numeric)0.05f){
			e1_x=0.5f;
			e1_y=-0.9f; //0.9~sqrt(3)/2
		}else if(position_y<(Numeric)-0.05f){
			e1_x=0.5f;
			e1_y=0.9f; //0.9~sqrt(3)/2
		}else{
			e1_x=1.0f;
		}
		/**************************/
	}else if(aim==TOWARD_EXIT){
		e1_y= 1;
	}
	new_velocity_x = e1_x * (desired_speed);
	new_velocity_y = e1_y * (desired_speed);

	force_x = (new_velocity_x - velocity_x) * (Numeric)1.0f/time_step;
	force_y = (new_velocity_y - velocity_y) * (Numeric)1.0f/time_step;
}

/*
This function is used to find out if two passengers are stuck because they are
 in a race for right of wayThen, it will try to give right of way to one of
 them as the winner of the race to resolve the issue.
*/

 __device__ void raceDetection(const int&__restrict ID, float*__restrict position_x, float*__restrict position_y, Numeric*__restrict velocity_x, Numeric*__restrict velocity_y, Numeric*__restrict force_x, Numeric*__restrict force_y,
 	int*__restrict race_status, Aim*__restrict aim, int*__restrict race_counter,  int*__restrict race_opID, float*__restrict race_position_x,float*__restrict race_position_y,  int *__restrict race_is_finished,
 	int*__restrict nearest_ID, Numeric*__restrict nearest_distance, curandState&__restrict curand_state, int*__restrict waitTime, Numeric*__restrict seat_position_x, bool*__restrict aisle_permission, Numeric*__restrict priority, const Numeric&__restrict time_step){
 	if((aim[ID]!=DOWN_TO_AISLE && aim[ID]!=UP_TO_AISLE) && aim[ID]!=OUT && (waitTime[ID]==0 && race_status[ID]!=LOOSER) &&  ((aim[ID]!=IN_AISLE && aim[ID]!=ALIGNING )||
 		(((position_x[ID]-seat_position_x[ID])<(Numeric)0.45f || (aisle_permission[-1*int(priority[ID])-1] ))))) {
 		Numeric speed = ((velocity_x[ID] * velocity_x[ID]) + (velocity_y[ID] * velocity_y[ID]));
 	Numeric force_mag = ((force_x[ID] * force_x[ID]) + (force_y[ID] * force_y[ID]));

 	if(speed<0.0001 && force_mag<0.0001){
 		if(race_status[ID]==NO_RACE){

 			int NID = nearest_ID[ID];
 			if(NID>=0){
 				if(race_status[NID]==NO_RACE && nearest_ID[NID]==ID){

 					atomicExch(&race_status[ID], IN_RACE);
 					atomicExch(&race_counter[ID], 1);
 					atomicExch(&race_opID[ID], NID);
 					atomicExch(&race_position_x[ID], position_x[ID]);
 					atomicExch(&race_position_y[ID], position_y[ID]);
 					atomicExch(&race_is_finished[ID], 0);


 					atomicExch(&race_status[NID], IN_RACE);
 					atomicExch(&race_counter[NID], 1);
 					atomicExch(&race_opID[NID], ID);
 					atomicExch(&race_position_x[NID], position_x[NID]);
 					atomicExch(&race_position_y[NID], position_y[NID]);
 					atomicExch(&race_is_finished[NID], 0);

 				}
 			}else{
				   // cout<<"ERROR 1: Passenger was not found: "<<NID<<" for "<<ID<<"\n";
 			}

			}else if(race_status[ID]==IN_RACE){ //Already in race
				if(race_opID[ID]==nearest_ID[ID]){
					atomicAdd(&race_counter[ID], 1);
				}else{

					int NID = nearest_ID[ID];
					int previous_opponent_ID = race_opID[ID];
					if(NID>=0 && previous_opponent_ID>=0){

						if(race_status[NID]==NO_RACE){

							atomicExch(&race_counter[ID], 1);
							atomicExch(&race_opID[ID], NID);
							atomicExch(&race_position_x[ID], position_x[ID]);
							atomicExch(&race_position_y[ID], position_y[ID]);
							atomicExch(&race_is_finished[ID], 0);


							atomicExch(&race_status[NID], IN_RACE);
							atomicExch(&race_counter[NID], 1);
							atomicExch(&race_opID[NID], ID);
							atomicExch(&race_position_x[NID], position_x[NID]);
							atomicExch(&race_position_y[NID], position_y[NID]);
							atomicExch(&race_is_finished[NID], 0);

							atomicExch(&race_status[previous_opponent_ID], NO_RACE);
							atomicExch(&race_opID[previous_opponent_ID], -10);
							atomicExch(&race_is_finished[previous_opponent_ID], 1);


						}else{
							//new opponent is in race
							//cout<<"New opponent is in race!\n";
						}
					}else{
						//cout<<"ERROR 2: Passenger was not found!\n";
					}
				}
				if(race_counter[ID]==(int)(1.5/time_step) && race_status[ID]==IN_RACE){
					//decide on the winner/looser
					//printf("%d: counter: %d is %d\n", ID, race_counter[ID], (int)(1500/time_step));
					int OpID = race_opID[ID];

					if(OpID>=0) {
						if(priority[ID] < priority[OpID]){
							atomicExch(&race_status[ID], WINNER);
							atomicExch(&race_status[OpID], LOOSER);
						}else if(priority[ID]>priority[OpID]){
							atomicExch(&race_status[OpID], WINNER);
							atomicExch(&race_status[ID], LOOSER);
						}else{
							if(aim[OpID]==UP_TO_AISLE || aim[OpID]==DOWN_TO_AISLE){
								atomicExch(&race_status[ID], WINNER);
								atomicExch(&race_status[OpID], LOOSER);
							}
							else if(aim[ID]==ALIGNING){
								if(aim[OpID]==ALIGNING){

									if(fabs(position_y[ID]) < fabs(position_y[OpID])){
										atomicExch(&race_status[ID], WINNER);
										atomicExch(&race_status[OpID], LOOSER);
									}else if(fabs(position_x[ID]) > fabs(position_x[OpID])){
										atomicExch(&race_status[OpID], WINNER);
										atomicExch(&race_status[ID], LOOSER);
									}else{
										if(position_x[ID] > position_x[OpID]){
											atomicExch(&race_status[ID], WINNER);
											atomicExch(&race_status[OpID], LOOSER);
										}else if(position_x[ID] < position_x[OpID]){
											atomicExch(&race_status[OpID], WINNER);
											atomicExch(&race_status[ID], LOOSER);
										}else{
											curandState localState=curand_state;
											float x = curand_uniform(&localState);
											curand_state = localState;
											if((int)(x*2)==0){
												//Winner
												atomicExch(&race_status[ID], WINNER);
												atomicExch(&race_status[OpID], LOOSER);
											}else{
												//Looser
												atomicExch(&race_status[OpID], WINNER);
												atomicExch(&race_status[ID], LOOSER);
											}
										}
									}
								}else{
									if(position_x[OpID] < position_x[ID]){
										atomicExch(&race_status[ID], WINNER);
										atomicExch(&race_status[OpID], LOOSER);
									}else{
										atomicExch(&race_status[OpID], WINNER);
										atomicExch(&race_status[ID], LOOSER);
									}

								}

							}else if(aim[OpID]==ALIGNING){
								if(position_x[ID] < position_x[OpID]){
									atomicExch(&race_status[OpID], WINNER);
									atomicExch(&race_status[ID], LOOSER);
								}else{
									atomicExch(&race_status[ID], WINNER);
									atomicExch(&race_status[OpID], LOOSER);
								}


							}else{
								if(position_x[ID] > position_x[OpID]){
									atomicExch(&race_status[ID], WINNER);
									atomicExch(&race_status[OpID], LOOSER);
								}else if(position_x[ID] < position_x[OpID]){
									atomicExch(&race_status[OpID], WINNER);
									atomicExch(&race_status[ID], LOOSER);
								}else{
									curandState localState=curand_state;
									float x = curand_uniform(&localState);
									curand_state = localState;
									if((int)(x*2)==0){
									//Winner
										atomicExch(&race_status[ID], WINNER);
										atomicExch(&race_status[OpID], LOOSER);
									}else{
										//Looser
										atomicExch(&race_status[OpID], WINNER);
										atomicExch(&race_status[ID], LOOSER);

										//passenger.setWaitTime(1000);
									}
								}
							}
						}

					}else{
						//cout<<"ERROR 3: Passenger was not found!\n";
					}
				}/*End if enough wait in race*/
			}//End if already in race
			else if(race_status[ID]==WINNER){
				//winner is blocked
				int first_looser = race_opID[ID];
				 int NID = nearest_ID[ID]; //itr
				 if(NID>=0 && first_looser>=0){
				 	if(race_status[NID]==WINNER){
				 		int second_looser= race_opID[NID];
				 		if(second_looser>=0){

				 			atomicExch(&race_counter[ID], 1);
				 			atomicExch(&race_status[NID], IN_RACE);
				 			atomicExch(&race_opID[ID], NID);
				 			atomicExch(&race_position_x[ID], position_x[ID]);
				 			atomicExch(&race_position_y[ID], position_y[ID]);
				 			atomicExch(&race_is_finished[ID], 0);

				 			atomicExch(&race_status[NID], IN_RACE);
				 			atomicExch(&race_counter[NID], 1);
				 			atomicExch(&race_opID[NID], ID);
				 			atomicExch(&race_position_x[NID], position_x[NID]);
				 			atomicExch(&race_position_y[NID], position_y[NID]);
				 			atomicExch(&race_is_finished[NID], 0);

				 			atomicExch(&race_status[first_looser], NO_RACE);
				 			atomicExch(&race_opID[first_looser], -10);
				 			atomicExch(&race_is_finished[first_looser], 1);

				 			atomicExch(&race_status[second_looser], NO_RACE);
				 			atomicExch(&race_opID[second_looser], -10);
				 			atomicExch(&race_is_finished[second_looser], 1);

				 		}else{
							//cout<<"ERROR 5: Passenger was not found!\n";
				 		}
				 	}else if(race_status[NID]==NO_RACE){

				 		atomicExch(&race_counter[ID], 1);
				 		atomicExch(&race_status[NID], IN_RACE);
				 		atomicExch(&race_opID[ID], NID);
				 		atomicExch(&race_position_x[ID], position_x[ID]);
				 		atomicExch(&race_position_y[ID], position_y[ID]);
				 		atomicExch(&race_is_finished[ID], 0);

				 		atomicExch(&race_status[NID], IN_RACE);
				 		atomicExch(&race_counter[NID], 1);
				 		atomicExch(&race_opID[NID], ID);
				 		atomicExch(&race_position_x[NID], position_x[NID]);
				 		atomicExch(&race_position_y[NID], position_y[NID]);
				 		atomicExch(&race_is_finished[NID], 0);

				 		atomicExch(&race_status[first_looser], NO_RACE);
				 		atomicExch(&race_opID[first_looser], -10);
				 		atomicExch(&race_is_finished[first_looser], 1);


				 	}else{
						//cout<<"Winner is still blocked! "<<itr->second.getint() <<"\n";
				 	}
				 }else{
				   // cout<<"ERROR 4: Passenger was not found!\n";
				 }


			}//end if winner
		}/*End if race condition*/
			else if(nearest_distance[ID] > 5 && race_status[ID] != NO_RACE){
				int opponent = race_opID[ID];
				if(opponent >=0){
					atomicExch(&race_status[ID], NO_RACE);
					atomicExch(&race_opID[ID], -10);
					atomicExch(&race_position_x[ID], position_x[ID]);
					atomicExch(&race_position_y[ID], position_y[ID]);
					atomicExch(&race_is_finished[ID], 1);

					atomicExch(&race_status[opponent], NO_RACE);
					atomicExch(&race_opID[opponent], -10);
					atomicExch(&race_is_finished[opponent], 1);

				}else{
				//cout<<"ERROR 6: Passenger was not found!\n";

				}
			}
	}//End IF
	//printf("raceDetection %d √s\n", ID);
}


/*
This function is used to call all other functions that are necessary for updating
force, velocity and position of passengers
*/
__device__ void UpdatePassengers( const int&__restrict i, int*__restrict IDs, int*__restrict waitTime, int*__restrict race_status, Aim*__restrict aim, Numeric*__restrict position_x, Numeric*__restrict position_y, Numeric*__restrict seat_position_x, bool*__restrict aisle_permission, int*__restrict nearest_ID, Numeric*__restrict nearest_position_x, Numeric*__restrict nearest_position_y, Numeric*__restrict nearest_distance, Numeric*__restrict priority, Numeric*__restrict obstacle_positions_x, Numeric*__restrict obstacle_positions_y,  int*__restrict race_opID, const Numeric&__restrict Aligning_Speed_Coefficient, const Numeric&__restrict Toward_Aisle_Speed_Coefficient, Numeric*__restrict velocity_x, Numeric*__restrict velocity_y, Numeric*__restrict force_x, Numeric*__restrict force_y, const Numeric&__restrict Reaction_Time, const Numeric&__restrict Intersection_Speed_Coefficient, const Numeric&__restrict Intersection_Distance_Threshold, const Numeric&__restrict Exit_X, Numeric*__restrict desiredSpeed, const Numeric&__restrict Time_Step, const Numeric&__restrict Aligning_Stop_Threshold, const Numeric&__restrict Toward_Aisle_Stop_Threshold, const Numeric&__restrict In_Aisle_Stop_Threshold, const Numeric&__restrict Cutoff_Threshold){
	bool condition3, condition4, condition5, condition6, condition7;
	Numeric condition1, condition2;
	condition1 = Numeric(aim[i]!=OUT && (waitTime[i]==0 && race_status[i]!=LOOSER) &&  ((aim[i]!=IN_AISLE && aim[i]!=ALIGNING )||(((position_x[i]-seat_position_x[i])<(Numeric)0.45f || (aisle_permission[-1*int(priority[i])-1] )))));
	condition3 = (race_status[i]==WINNER && nearest_ID[i]==race_opID[i]);
	condition4 = aim[i]==UP_TO_AISLE || aim[i]==DOWN_TO_AISLE ;

	condition6 = aim[i]==IN_AISLE && position_x[i]+ Intersection_Distance_Threshold>Exit_X;
	//if(condition1){
	Numeric DesiredSpeed;

	bool obs_block=blockedPath(IDs[i], obstacle_positions_x, obstacle_positions_y, position_x[i], position_y[i], aim[i]);
	condition2 = Numeric(obs_block && nearest_distance[i]>0.1f);
	condition5 = nearest_distance[i]<=(Numeric)0.2f || obs_block;

	nearest_distance[i]= nearest_distance[i]*(Numeric(1.0f) - condition2) + condition2 * Numeric(0.1f);



	if(condition3){
			//DesiredSpeed =DecreaseSpeed(desiredSpeed[i], Aligning_Stop_Threshold*(Numeric)0.8f, nearest_distance[i]);
		DesiredSpeed =DecreaseSpeed((Numeric)1.1f, nearest_distance[i], desiredSpeed[i]);
	}else if(aim[i]==ALIGNING){
			//DesiredSpeed =DecreaseSpeed(desiredSpeed[i], Aligning_Stop_Threshold, nearest_distance[i]) * Aligning_Speed_Coefficient;
		DesiredSpeed =DecreaseSpeed(Aligning_Speed_Coefficient, nearest_distance[i], desiredSpeed[i]);
	}else if(condition4){
			//DesiredSpeed =DecreaseSpeed(desiredSpeed[i], Toward_Aisle_Stop_Threshold, nearest_distance[i]) * Toward_Aisle_Speed_Coefficient ;
		DesiredSpeed =DecreaseSpeed(Toward_Aisle_Speed_Coefficient, nearest_distance[i], desiredSpeed[i]) ;
	}
	else
		DesiredSpeed =DecreaseSpeed((Numeric)1.0f, nearest_distance[i], desiredSpeed[i]);
			//DesiredSpeed =DecreaseSpeed(desiredSpeed[i], In_Aisle_Stop_Threshold, nearest_distance[i]);


		/*if(condition5){
			force_x[i] = velocity_x[i]*(-20.0f);
			force_y[i] = velocity_y[i]*(-20.0f);
		}
		else {*/

		if(condition6){
			Propulsive_Force(aim[i], position_y[i], velocity_x[i], velocity_y[i], Reaction_Time, DesiredSpeed*Intersection_Speed_Coefficient, force_x[i], force_y[i]);
		}else{
			Propulsive_Force(aim[i], position_y[i], velocity_x[i], velocity_y[i], Reaction_Time, DesiredSpeed, force_x[i], force_y[i]);
			condition7 = (fabs(position_y[i])<0.15f && velocity_x[i]<=0.0f && force_x[i]<0.0f);
			if(condition7){
				checkBehind(IDs, IDs[i], position_x, position_y, velocity_x[i], velocity_y[i], force_x[i], force_y[i]);
			}
		}

		//}//end else
		velocity_x[i] = velocity_x[i] * condition1;
		velocity_y[i] = velocity_y[i] * condition1;
		force_x[i] = force_x[i] * condition1;
		force_y[i] = force_y[i] * condition1;
		EulerMethod(race_status[i], force_x[i], force_y[i], velocity_x[i], velocity_y[i], position_x[i], position_y[i], aim[i], seat_position_x[i], desiredSpeed[i], Time_Step);


	//}//End initial if
		if(waitTime[i]!=0){
			waitTime[i] = waitTime[i]-1;
		}
	}

/*
This function updates passengers state and flags like aim based on their updated position
*/
__device__ void UpdatePassengersFlags(const int&__restrict ID, Aim*__restrict aim, Numeric*__restrict position_x, Numeric*__restrict position_y, const Numeric&__restrict luggage_location_y, int*__restrict wait_time, bool*__restrict has_released, Numeric*__restrict seat_position_x, int*__restrict holds, Numeric*__restrict priority, bool*__restrict aisle_permission, const Numeric&__restrict Exit_X, const Numeric&__restrict Exit_Y, int*__restrict race_status,  int*__restrict race_opID,  int*__restrict race_finished, Numeric*__restrict race_position_x, Numeric*__restrict race_position_y, const Numeric&__restrict time_step,  int&__restrict numberOfPassengersInPlane, const Numeric&__restrict Release_Threshold, curandState&__restrict curand_state){
	
	bool condition1, condition2, condition3;
	condition1 = (aim[ID]==DOWN_TO_AISLE)&&(position_y[ID]<luggage_location_y-(Numeric)0.2f);
	condition2 = (aim[ID]==UP_TO_AISLE)&&(position_y[ID]> (-1*luggage_location_y) + (Numeric)0.2f);
	condition3 = (aim[ID]==TOWARD_EXIT)&&(position_y[ID] > Exit_Y+(Numeric)0.5f);

	if(condition1){
		/*passenger has moved down enough toward aisle*/
		aim[ID] = ALIGNING;
		/*time to wait for baggage loading*/
		curandState localState=curand_state;
		float x = curand_uniform(&localState);
		curand_state = localState;
		wait_time[ID] = ((int)(5.0f/time_step)+(int)(x*((int)(7.0f/time_step))));

	}else if(condition2){
		/*passenger has moved up enough toward aisle*/
		aim[ID] = ALIGNING;
		/*time to wait for baggage loading*/
		curandState localState=curand_state;
		float x = curand_uniform(&localState);
		curand_state = localState;
		wait_time[ID] = ((int)(5.0f/time_step)+(int)(x*((int)(7.0f/time_step))));

	}else if(aim[ID]==ALIGNING){
		if(fabs(position_y[ID])<=(Numeric)0.06f){
			aim[ID] = IN_AISLE;
		}
	}else if(aim[ID]==IN_AISLE){
		if(!has_released[ID] && position_x[ID]-seat_position_x[ID]>Release_Threshold){
		  /*Passenger has moved more than "Release_Threshold" in the aisle and has not yet released the hold.
		So, it is time to release the hold and check to see if it is the last hold. In that case, we have to get aisle permission to the next row*/
			has_released[ID] = (true);
			atomicSub(&holds[-1*int(priority[ID])-1], 1);
			//holds[-1*int(priority[ID])-1]--;
			if(holds[-1*int(priority[ID])-1]==0 && (-1*int(priority[ID])-2)>=0){
				aisle_permission[-1*int(priority[ID])-2]=true;
			}
		}
		if(position_x[ID] > Exit_X+(Numeric)0.05f){
		  /*Time to urn toward the exit door*/
			aim[ID] = (TOWARD_EXIT);
			if( !has_released[ID]){
				has_released[ID] = (true);
				//holds[-1*int(priority[ID])-1]--;
				atomicSub(&holds[-1*int(priority[ID])-1], 1);
				if(holds[-1*int(priority[ID])-1]==0){
					aisle_permission[-1*int(priority[ID])-2]=true;
				}
			}
		}
			/*Checking to see the winner has moved enough in the aisle so that we can remove the looser tag from the looser passenger*/
		if(race_status[ID]==WINNER && race_opID[ID] >= 0 && (race_finished[ID]!=1)){
			if(position_x[ID] - race_position_x[ID] >1 && race_status[race_opID[ID]]==LOOSER){
				race_status[race_opID[ID]] = NO_RACE;
				race_opID[race_opID[ID]] = -10;
				race_finished[race_opID[ID]] = 1;
				race_status[ID] = NO_RACE;
				race_opID[ID]=-10;
				race_finished[ID]=1;
			}

		}
	}else if(condition3){
		/*
		Passenger has reached the exit door
		*/
		atomicSub(&numberOfPassengersInPlane, 1);

		aim[ID] =OUT;

	}

	//printf("UpdatePassengersFlags %d √\n", ID);

}

__device__ void savePositions(Numeric*__restrict positions_x, Numeric*__restrict positions_y, Numeric*__restrict output, const int& n){
	for(int i=0; i<NumberOfPassengers; ++i){
		output[n *2* NumberOfPassengers + i*2] = positions_x[i];
		output[n *2* NumberOfPassengers + i*2 +1] = positions_y[i];
	}
	output[(n+1)*2*NumberOfPassengers]=-1000.0;
	output[(n+1)*2*NumberOfPassengers+1]=-1000.0;
}

__device__ void WriteOutput(Numeric*__restrict position_x, Numeric*__restrict position_y, const int& n, const char*__restrict xyz_obs){

	int coord_count = NumberOfObstacles + NumberOfPassengers;
	printf(" %d \n   %d\n", coord_count, (n * 250));
	for(int its=0; its<NumberOfPassengers; ++its){

		//converting meter to inches
		double x_pos = position_x[its]*39.3701f;
		double y_pos = position_y[its]*39.3701f;

		printf("C%12.3f%12.3f       0.000\n", x_pos, y_pos);

	}
	/*if(BlockID == 0 && n==1)
		printf("%s\n", xyz_obs);*/

}


void WriteToTextFile(Numeric*__restrict data, const char*__restrict xyz_obs, const Numeric& Time_Step, const int& Output_Iteration, int*__restrict iteration_counter){
	int coord_count = NumberOfObstacles + NumberOfPassengers;
	for(int i=0; i<NumberOfBlocks; ++i){
		char file_name[32];
		FILE* file_descriptor;
		sprintf(file_name, "anim_%d", i);
		file_descriptor = fopen (file_name, "w");
		if(file_descriptor == NULL)
			cout<<"Error openning"<<file_name<<" file\n";
		else{
			int index=i*550*2*NumberOfPassengers, local_index=0, saved_sets = iteration_counter[i]/Output_Iteration;

			//while(data[index]!=-1000.0){
			for(int k=0; k<saved_sets; ++k){
				fprintf(file_descriptor, " %d \n   %d\n", coord_count, ((local_index+1) * 250));

				for(int its=0; its<NumberOfPassengers; ++its){
					//converting meter to inches
					double x_pos = data[index + its*2]*39.3701f;
					double y_pos = data[index + its*2 +1]*39.3701f;
					fprintf(file_descriptor, "C%12.3f%12.3f       0.000\n", x_pos, y_pos);
				}
				fprintf(file_descriptor, "%s\n", xyz_obs);
				++local_index;
				index += 2*144;
			}//END WHILE
			//fprintf(file_descriptor, "%d$SM= %f, ASC=%f, TASC= %f, ISC= %f, IDT= %f, RT=%f\n", i, Spead_Mean, s_Aligning_Speed_Coefficient[0], s_Toward_Aisle_Speed_Coefficient[0], s_Intersection_Speed_Coefficient[0], s_Intersection_Distance_Threshold[0], s_Release_Threshold[0]);
			fprintf(file_descriptor, "Physical time: %f (s) = %f minutes\n",(Numeric)(iteration_counter[i]*Time_Step), (Numeric)(iteration_counter[i]*Time_Step)/60.0f);
			fclose(file_descriptor);
		}//END ELSE
	}//END FOR
}


void WriteToBinaryFile(Numeric*__restrict data, const char*__restrict xyz_obs, const Numeric& Time_Step, const int& Output_Iteration, int*__restrict iteration_counter){
	int coord_count = NumberOfObstacles + NumberOfPassengers;

	char file_name[32];
	FILE* file_descriptor;
	sprintf(file_name, "anim");
	file_descriptor = fopen (file_name, "wb");
	if(file_descriptor == NULL)
		cout<<"Error openning"<<file_name<<" file\n";
	else{
		for(int i=0; i<NumberOfBlocks; ++i){

			int index=i*550*2*144, local_index=0, saved_sets = iteration_counter[i]/Output_Iteration;
			fwrite(&saved_sets, sizeof(int), 1, file_descriptor);
			fwrite(&data[index], saved_sets*sizeof(Numeric)*2*144, 1, file_descriptor);
			int size = strlen(xyz_obs);
			//printf("size of obs: %d\n", size);
			fwrite(&size, sizeof(int), 1, file_descriptor);
			fwrite(xyz_obs, size, 1, file_descriptor);
			fwrite(&iteration_counter[i], sizeof(int), 1, file_descriptor);
		}//END FOR
	}//END ELSE
	fclose(file_descriptor);
}



__global__ void RunWithSM (int*__restrict iteration_counter, int&__restrict numberOfPassengersInPlane, const Numeric&__restrict Exit_X, const Numeric&__restrict Exit_Y, Aim*__restrict aim, Numeric*__restrict position_x, Numeric*__restrict position_y, int*__restrict nearest_ID, Numeric*__restrict nearest_position_x, Numeric*__restrict nearest_position_y, Numeric*__restrict nearest_distance,  int*__restrict IDs, int*__restrict waitTime, int*__restrict race_status, Numeric*__restrict seat_position_x, bool *__restrict aisle_permission, Numeric*__restrict priority, Numeric*__restrict obs_positions_x, Numeric*__restrict obs_positions_y,  int*__restrict race_opID, Numeric*__restrict velocity_x, Numeric*__restrict velocity_y, Numeric*__restrict force_x, Numeric*__restrict force_y, const Numeric&__restrict Reaction_Time, Numeric*__restrict desiredSpeed, const Numeric&__restrict Time_Step,  int*__restrict race_finished, int*__restrict race_counter, Numeric*__restrict race_position_x, Numeric*__restrict race_position_y, const Numeric&__restrict Luggage_Y, bool*__restrict has_released, int*__restrict holds, const Numeric&__restrict Aligning_Stop_Threshold, const Numeric&__restrict Toward_Aisle_Stop_Threshold, const Numeric&__restrict In_Aisle_Stop_Threshold, const Numeric&__restrict Output_Iteration, const Numeric&__restrict Number_Of_Outputs, const char*__restrict xyz_obs, const int&__restrict Total_Number_Of_Rows, Numeric* parameters, Numeric *outputs){
	extern __shared__ Numeric shared_data[];

	Numeric Cutoff_Threshold=2.0f;
	int Outputindex=0, ID = threadIdx.x, Param_Index=blockIdx.x;

	Numeric* this_output = outputs + Param_Index * 550 * 2 * NumberOfPassengers;


	/*if(ID==0){
	for(int i=0; i<550*144*2; ++i){
			if(this_output[i]!=-1000.0){
		printf("WRONG SET in %d!\n", i);
				//exit(0);
			return;
			}
	}
	}*/

	Numeric* s_obs_positions_x = (Numeric *) shared_data;
	Numeric* s_obs_positions_y = (Numeric *) (shared_data + NumberOfObstacles);
	Numeric* s_position_x = (Numeric *) (shared_data + 2 * NumberOfObstacles);
	Numeric* s_position_y = (Numeric *) (shared_data + 2 * NumberOfObstacles + NumberOfPassengers);
	//Numeric* s_mass = (Numeric *) (shared_data + 2 * NumberOfObstacles + 2 * NumberOfPassengers);
	Numeric* s_priority = (Numeric *) (shared_data + 2 * NumberOfObstacles + 3 * NumberOfPassengers);
	Numeric* s_race_position_x = (Numeric *) (shared_data + 2 * NumberOfObstacles + 4 * NumberOfPassengers);
	Numeric* s_race_position_y = (Numeric *) (shared_data + 2 * NumberOfObstacles + 5 * NumberOfPassengers);
	Numeric* s_velocity_x = (Numeric *) (shared_data + 2 * NumberOfObstacles + 6 * NumberOfPassengers);
	Numeric* s_velocity_y = (Numeric *) (shared_data + 2 * NumberOfObstacles + 7 * NumberOfPassengers);
	Numeric* s_force_x = (Numeric *) (shared_data + 2 * NumberOfObstacles + 8 * NumberOfPassengers);
	Numeric* s_force_y = (Numeric *) (shared_data + 2 * NumberOfObstacles + 9 * NumberOfPassengers);
	Numeric* s_seat_position_x = (Numeric *) (shared_data + 2 * NumberOfObstacles + 10 * NumberOfPassengers);
	Numeric* s_desiredSpeed = (Numeric *) (shared_data + 2 * NumberOfObstacles + 11 * NumberOfPassengers);
	Numeric* s_nearest_position_x = (Numeric *) (shared_data + 2 * NumberOfObstacles + 12 * NumberOfPassengers);
	Numeric* s_nearest_position_y = (Numeric *) (shared_data + 2 * NumberOfObstacles + 13 * NumberOfPassengers);
	Numeric* s_nearest_distance = (Numeric *) (shared_data + 2 * NumberOfObstacles + 14 * NumberOfPassengers);
	Numeric* s_Exit_X = (Numeric *) (shared_data + 2 * NumberOfObstacles + 15 * NumberOfPassengers);
	Numeric* s_Exit_Y = (Numeric *) (shared_data + 2 * NumberOfObstacles + 15 * NumberOfPassengers + 1);
	Numeric* s_Aligning_Speed_Coefficient = (Numeric *) (shared_data + 2 * NumberOfObstacles + 15 * NumberOfPassengers + 2);
	Numeric* s_Toward_Aisle_Speed_Coefficient = (Numeric *) (shared_data + 2 * NumberOfObstacles + 15 * NumberOfPassengers + 3);
	Numeric* s_Reaction_Time = (Numeric *) (shared_data + 2 * NumberOfObstacles + 15 * NumberOfPassengers + 4);
	Numeric* s_Intersection_Speed_Coefficient = (Numeric *) (shared_data + 2 * NumberOfObstacles + 15 * NumberOfPassengers + 5);
	Numeric* s_Intersection_Distance_Threshold = (Numeric *) (shared_data + 2 * NumberOfObstacles + 15 * NumberOfPassengers + 6);
	Numeric* s_Time_Step = (Numeric *) (shared_data + 2 * NumberOfObstacles + 15 * NumberOfPassengers + 7);
	Numeric* s_Luggage_Y = (Numeric *) (shared_data + 2 * NumberOfObstacles + 15 * NumberOfPassengers + 8);
	Numeric* s_Release_Threshold = (Numeric *) (shared_data + 2 * NumberOfObstacles + 15 * NumberOfPassengers + 9);
	Numeric* s_Aligning_Stop_Threshold = (Numeric *) (shared_data + 2 * NumberOfObstacles + 15 * NumberOfPassengers + 10);
	Numeric* s_Toward_Aisle_Stop_Threshold = (Numeric *) (shared_data + 2 * NumberOfObstacles + 15 * NumberOfPassengers + 11);
	Numeric* s_In_Aisle_Stop_Threshold = (Numeric *) (shared_data + 2 * NumberOfObstacles + 15 * NumberOfPassengers + 12);

	int* s_Output_Iteration = (int *) (shared_data + 2 * NumberOfObstacles + 15 * NumberOfPassengers + 13);
	int* s_Number_Of_Outputs = (int *) (shared_data + 2 * NumberOfObstacles + 15 * NumberOfPassengers + 14);
	int* s_iteration_counter = (int *) (shared_data + 2 * NumberOfObstacles + 15 * NumberOfPassengers + 15);
	int* s_numberOfPassengersInPlane = (int *) (shared_data + 2 * NumberOfObstacles + 15 * NumberOfPassengers + 16);
	int* s_IDs = (int *) (shared_data + 2 * NumberOfObstacles + 15 * NumberOfPassengers + 17);
	int* s_nearest_ID = (int *) (shared_data + 2 * NumberOfObstacles + 16 * NumberOfPassengers + 17);
	int* s_race_opID = (int *) (shared_data + 2 * NumberOfObstacles + 17 * NumberOfPassengers + 17);
	int* s_race_finished = (int *) (shared_data + 2 * NumberOfObstacles + 18 * NumberOfPassengers + 17);
	int* s_race_counter = (int *) (shared_data + 2 * NumberOfObstacles + 19 * NumberOfPassengers + 17);
	int* s_race_status = (int *) (shared_data + 2 * NumberOfObstacles + 20 * NumberOfPassengers + 17);
	int* s_waitTime = (int *) (shared_data + 2 * NumberOfObstacles + 21 * NumberOfPassengers + 17);
	int* s_holds = (int *) (shared_data + 2 * NumberOfObstacles + 22 * NumberOfPassengers + 17);

	Aim* s_aim = (Aim *) (shared_data + 2 * NumberOfObstacles + 22 * NumberOfPassengers + 17 + Total_Number_Of_Rows);
	bool* s_aisle_permission = (bool *) (shared_data + 2 * NumberOfObstacles + 23 * NumberOfPassengers + 17 + Total_Number_Of_Rows);
	bool* s_has_released = (bool *) (shared_data + 2 * NumberOfObstacles + 23 * NumberOfPassengers + 17 + 2*Total_Number_Of_Rows);

	Numeric std_dev = 0.2f, min_SM = 1.1f, min_IDT = 0.2f, min_ISC = 0.2f, min_ASC = 0.2f, min_TASC = 0.2f, min_RT = 0.5f, max_SM = 1.3f, max_IDT = 1.5f, max_ISC = 0.8f, max_ASC = 0.7f, max_TASC = 0.6f, max_RT = 1.6f;
	Numeric Spead_Mean, Intersection_Distance_Threshold, Intersection_Speed_Coefficient, Aligning_Speed_Coefficient, Toward_Aisle_Speed_Coefficient, Release_Threshold;

	Spead_Mean = (max_SM - min_SM) * parameters[Param_Index*ParametersPerSimulation] + min_SM;
	Intersection_Distance_Threshold = (max_IDT - min_IDT) * parameters[Param_Index*ParametersPerSimulation + 1] + min_IDT;
	Intersection_Speed_Coefficient = (max_ISC - min_ISC) * parameters[Param_Index*ParametersPerSimulation + 2] + min_ISC;
	Aligning_Speed_Coefficient = (max_ASC - min_ASC) * parameters[Param_Index*ParametersPerSimulation + 3] + min_ASC;
	Toward_Aisle_Speed_Coefficient = (max_TASC - min_TASC) * parameters[Param_Index*ParametersPerSimulation + 4] + min_TASC;
	Release_Threshold = (max_RT - min_RT) * parameters[Param_Index*ParametersPerSimulation + 5] + min_RT;


	//printf("%d √\n",ID);
	if(ID==0){
		s_iteration_counter[0] = 0;
		s_numberOfPassengersInPlane[0] = numberOfPassengersInPlane;
		s_Exit_X[0] = Exit_X;
		s_Exit_Y[0] = Exit_Y;
		s_Aligning_Speed_Coefficient[0] = Aligning_Speed_Coefficient;
		s_Toward_Aisle_Speed_Coefficient[0] = Toward_Aisle_Speed_Coefficient;
		s_Reaction_Time[0] = Reaction_Time;
		s_Intersection_Speed_Coefficient[0] = Intersection_Speed_Coefficient;
		s_Intersection_Distance_Threshold[0] = Intersection_Distance_Threshold;
		s_Time_Step[0] = Time_Step;
		s_Luggage_Y[0] = Luggage_Y;
		s_Release_Threshold[0] = Release_Threshold;
		s_Aligning_Stop_Threshold[0] = Aligning_Stop_Threshold;
		s_Toward_Aisle_Stop_Threshold[0] = Toward_Aisle_Stop_Threshold;
		s_In_Aisle_Stop_Threshold[0] = In_Aisle_Stop_Threshold;
		s_Output_Iteration[0] = Output_Iteration;
		s_Number_Of_Outputs[0] = Number_Of_Outputs;
	}
	/*if(ID==0)
	printf("%d$SM= %f, ASC=%f, TASC= %f, ISC= %f, IDT= %f, RT=%f\n", Param_Index, Spead_Mean, s_Aligning_Speed_Coefficient[0], s_Toward_Aisle_Speed_Coefficient[0], s_Intersection_Speed_Coefficient[0], s_Intersection_Distance_Threshold[0], s_Release_Threshold[0]);*/

	//ASSUMPTION: #THREADS >= #ROWS
	if(ID<Total_Number_Of_Rows){
		s_holds[ID] = holds[ID];
		s_aisle_permission[ID] = aisle_permission[ID];
	}

	int obstacles_per_thread = int(NumberOfObstacles / NumberOfThreads) +1;
	for(int f=0; f<obstacles_per_thread && ID*obstacles_per_thread+f<NumberOfObstacles; ++f){
		s_obs_positions_x[ID*obstacles_per_thread+f] = obs_positions_x[ID*obstacles_per_thread+f];
		s_obs_positions_y[ID*obstacles_per_thread+f] = obs_positions_y[ID*obstacles_per_thread+f];
	}

	int passengers_per_thread = int(NumberOfPassengers / NumberOfThreads) +1;

	for(int i=0; i<passengers_per_thread && ID*passengers_per_thread+i<NumberOfPassengers; ++i){
		int passenger_ID = ID * passengers_per_thread + i;
		s_position_x[passenger_ID] =   position_x[passenger_ID];
		s_position_y[passenger_ID] =   position_y[passenger_ID];
		//s_mass[passenger_ID]       =   mass[passenger_ID];
		s_priority[passenger_ID]   =   priority[passenger_ID];
		s_waitTime[passenger_ID]   =   waitTime[passenger_ID];
		s_aim[passenger_ID]        =   aim[passenger_ID];
		s_velocity_x[passenger_ID] =   velocity_x[passenger_ID];
		s_velocity_y[passenger_ID] =   velocity_y[passenger_ID];
		s_force_x[passenger_ID]    =   force_x[passenger_ID];
		s_force_y[passenger_ID]    =   force_y[passenger_ID];
		s_seat_position_x[passenger_ID] =   seat_position_x[passenger_ID];
		s_has_released[passenger_ID]    =   has_released[passenger_ID];
		//s_desiredSpeed[passenger_ID]    =   desiredSpeed[passenger_ID];
		curandState normal_state;
		curand_init(1234, passenger_ID, 0, &normal_state);
		Numeric speed = 0;
		while(speed<0.7 || speed>1.6){
			speed = curand_normal(&normal_state);
		}
		s_desiredSpeed[passenger_ID] = speed * std_dev + Spead_Mean;

		s_IDs[passenger_ID]        =   IDs[passenger_ID];
		s_race_position_x[passenger_ID] =   race_position_x[passenger_ID];
		s_race_position_y[passenger_ID] =   race_position_y[passenger_ID];
		s_nearest_ID[passenger_ID] =   nearest_ID[passenger_ID];
		s_nearest_position_x[passenger_ID] =   nearest_position_x[passenger_ID];
		s_nearest_position_y[passenger_ID] =   nearest_position_y[passenger_ID];
		s_nearest_distance[passenger_ID]   =   nearest_distance[passenger_ID];
		s_race_opID[passenger_ID]  =   race_opID[passenger_ID];
		s_race_finished[passenger_ID] =   race_finished[passenger_ID];
		s_race_counter[passenger_ID]  =   race_counter[passenger_ID];
		s_race_status[passenger_ID]   =   race_status[passenger_ID];

	}

	__syncthreads();



	while(s_numberOfPassengersInPlane[0]>2){
		if(ID==0){
			++s_iteration_counter[0];
		}

		for(int i=0; i<passengers_per_thread && ID*passengers_per_thread+i<NumberOfPassengers; ++i){
			int passenger_ID = ID * passengers_per_thread + i;
			curandState state;
			curand_init(1234, passenger_ID, 0, &state);
			getNearestPassenger(s_IDs[passenger_ID], s_Exit_X[0], s_aim, s_position_x, s_position_y, s_nearest_ID[passenger_ID], s_nearest_position_x[passenger_ID], s_nearest_position_y[passenger_ID], s_nearest_distance[passenger_ID]);

			__syncthreads();

			UpdatePassengers(s_IDs[passenger_ID], s_IDs, s_waitTime, s_race_status, s_aim, s_position_x, s_position_y, s_seat_position_x, s_aisle_permission, s_nearest_ID, s_nearest_position_x, s_nearest_position_y, s_nearest_distance, s_priority, s_obs_positions_x, s_obs_positions_y, s_race_opID, s_Aligning_Speed_Coefficient[0], s_Toward_Aisle_Speed_Coefficient[0], s_velocity_x, s_velocity_y, s_force_x, s_force_y, s_Reaction_Time[0], s_Intersection_Speed_Coefficient[0], s_Intersection_Distance_Threshold[0], s_Exit_X[0], s_desiredSpeed, s_Time_Step[0], s_Aligning_Stop_Threshold[0], s_Toward_Aisle_Stop_Threshold[0], s_In_Aisle_Stop_Threshold[0], Cutoff_Threshold);

		  /*DEBUG:
		  //cout<<endl;
			//for(int i=0; i<NumberOfPassengers; ++i){
				if((ID==132 || ID==134 || ID==135) && s_aim[passenger_ID]!= OUT){
					printf("%d: position(%f, %f) & nearest in (%f, %f) with distance %f is %d & force(%f, %f) & velocity(%f, %f) & s_aim:%d, Race: %d, Wait: %f, s_priority: %f\n", ID, position_x[passenger_ID], position_y[passenger_ID], s_nearest_position_x[passenger_ID], s_nearest_position_y[passenger_ID], s_nearest_distance[passenger_ID], s_nearest_ID[passenger_ID], s_force_x[passenger_ID], s_force_y[passenger_ID], s_velocity_x[passenger_ID], s_velocity_y[passenger_ID], s_aim[passenger_ID], s_race_status[passenger_ID], s_waitTime[passenger_ID], s_priority[passenger_ID]);
					if(s_race_status[passenger_ID]!=NO_RACE){
						printf("%d: Opponent: %d, counter: %d\n", ID, s_race_opID[passenger_ID], s_race_counter[passenger_ID]);
					}else{
						printf("\n");
					}
				}
		   // }/**/

				raceDetection(s_IDs[passenger_ID], (float*) s_position_x, (float *)s_position_y, s_velocity_x, s_velocity_y, s_force_x, s_force_y, s_race_status, s_aim, s_race_counter, s_race_opID, (float *)s_race_position_x, (float *) s_race_position_y, s_race_finished, s_nearest_ID, s_nearest_distance, state, s_waitTime, s_seat_position_x, s_aisle_permission, s_priority, s_Time_Step[0]);

				UpdatePassengersFlags(s_IDs[passenger_ID], s_aim, s_position_x, s_position_y, s_Luggage_Y[0], s_waitTime, s_has_released, s_seat_position_x, s_holds, s_priority, s_aisle_permission, s_Exit_X[0], s_Exit_Y[0], s_race_status, s_race_opID, s_race_finished, s_race_position_x, s_race_position_y, s_Time_Step[0], s_numberOfPassengersInPlane[0], s_Release_Threshold[0], state);

				s_force_x[passenger_ID] = (Numeric)0.0f;
				s_force_y[passenger_ID] = (Numeric)0.0f;
				s_nearest_distance[passenger_ID] = (Numeric)100.0f;

		}//END FOR
		if(Outputindex%int(s_Output_Iteration[0])==s_Output_Iteration[0]-2 && ID==0){
			//printf("This is n: %d\n", Outputindex/s_Output_Iteration[0]);
			savePositions(s_position_x, s_position_y, this_output, Outputindex/s_Output_Iteration[0]);
			//WriteOutput(s_position_x, s_position_y, 1+Outputindex/s_Output_Iteration[0], xyz_obs);
		}
		Outputindex++;
		if(Outputindex>s_Output_Iteration[0]*(s_Number_Of_Outputs[0]-1)){
			break;
		}

	}//End While
	if(ID==0){
		iteration_counter[Param_Index] = s_iteration_counter[0];
	}
		/*printf("%d$SM= %f, ASC=%f, TASC= %f, ISC= %f, IDT= %f, RT=%f\n", Param_Index, Spead_Mean, s_Aligning_Speed_Coefficient[0], s_Toward_Aisle_Speed_Coefficient[0], s_Intersection_Speed_Coefficient[0], s_Intersection_Distance_Threshold[0], s_Release_Threshold[0]);
		printf("%d$Physical time: %f (s) = %f minutes\n", Param_Index, (Numeric)(s_iteration_counter[0]*s_Time_Step[0]), (Numeric)(s_iteration_counter[0]*s_Time_Step[0])/60.0f);
	}*/
	}



	__global__ void RunWithoutSM (int*__restrict iteration_counter, int*__restrict numberOfPassengersInPlane, const Numeric&__restrict Exit_X, const Numeric&__restrict Exit_Y, Aim*__restrict aim, Numeric*__restrict position_x, Numeric*__restrict position_y, int*__restrict nearest_ID, Numeric*__restrict nearest_position_x, Numeric*__restrict nearest_position_y, Numeric*__restrict nearest_distance,  int*__restrict IDs, int*__restrict waitTime, int*__restrict race_status, Numeric*__restrict seat_position_x, bool *__restrict aisle_permission, Numeric*__restrict priority, Numeric*__restrict obs_positions_x, Numeric*__restrict obs_positions_y,  int*__restrict race_opID, Numeric*__restrict velocity_x, Numeric*__restrict velocity_y, Numeric*__restrict force_x, Numeric*__restrict force_y, const Numeric&__restrict Reaction_Time, Numeric*__restrict desiredSpeed, const Numeric&__restrict Time_Step,  int*__restrict race_finished, int*__restrict race_counter, Numeric*__restrict race_position_x, Numeric*__restrict race_position_y, const Numeric&__restrict Luggage_Y, bool*__restrict has_released, int*__restrict holds, const Numeric&__restrict Aligning_Stop_Threshold, const Numeric&__restrict Toward_Aisle_Stop_Threshold, const Numeric&__restrict In_Aisle_Stop_Threshold, const Numeric&__restrict Output_Iteration, const Numeric&__restrict Number_Of_Outputs, const int&__restrict Total_Number_Of_Rows, Numeric* parameters, Numeric *outputs, int*__restrict SimIDs){

		Numeric previousPassengersInPlane;
		const Numeric Cutoff_Threshold = (Numeric)2.0f;
		int Outputindex=0, Param_Index=SimIDs[blockIdx.x], BlockID = blockIdx.x, ID = threadIdx.x;
		int passengers_per_thread = int(ceil((float)((float)(NumberOfPassengers) / (float) NumberOfThreads )));
		//Numeric std_dev = 0.2f, min_SM = 1.15f, min_IDT = 0.2f, min_ISC = 0.2f, min_ASC = 0.2f, min_TASC = 0.1f, min_RT = 0.5f, max_SM = 1.3f, max_IDT = 1.5f, max_ISC = 0.9f, max_ASC = 0.9f, max_TASC = 0.7f, max_RT = 2.0f;
		Numeric std_dev = 0.2f, min_SM = 1.1f, min_IDT = 0.2f, min_ISC = 0.2f, min_ASC = 0.2f, min_TASC = 0.2f, min_RT = 0.5f, max_SM = 1.3f, max_IDT = 1.5f, max_ISC = 0.8f, max_ASC = 0.7f, max_TASC = 0.6f, max_RT = 1.6f;
		Numeric Spead_Mean, Intersection_Distance_Threshold, Intersection_Speed_Coefficient, Aligning_Speed_Coefficient, Toward_Aisle_Speed_Coefficient, Release_Threshold;

		Numeric* this_output = outputs + BlockID * 550 * 2 * NumberOfPassengers;

		Numeric* t_position_x = (Numeric *) (position_x + BlockID * NumberOfPassengers);
		Numeric* t_position_y = (Numeric *) (position_y + BlockID * NumberOfPassengers);
	//Numeric* t_mass = (Numeric *) (mass + BlockID * NumberOfPassengers);
		Numeric* t_priority = (Numeric *) (priority + BlockID * NumberOfPassengers);
		Numeric* t_race_position_x = (Numeric *) (race_position_x + BlockID * NumberOfPassengers);
		Numeric* t_race_position_y = (Numeric *) (race_position_x + BlockID * NumberOfPassengers);
		Numeric* t_velocity_x = (Numeric *) (velocity_x + BlockID * NumberOfPassengers);
		Numeric* t_velocity_y = (Numeric *) (velocity_y + BlockID * NumberOfPassengers);
		Numeric* t_force_x = (Numeric *) (force_x + BlockID * NumberOfPassengers);
		Numeric* t_force_y = (Numeric *) (force_y + BlockID * NumberOfPassengers);
		Numeric* t_seat_position_x = (Numeric *) (seat_position_x + BlockID * NumberOfPassengers);
		Numeric* t_desiredSpeed = (Numeric *) (desiredSpeed + BlockID * NumberOfPassengers);
		Numeric* t_nearest_position_x = (Numeric *) (nearest_position_x + BlockID * NumberOfPassengers);
		Numeric* t_nearest_position_y = (Numeric *) (nearest_position_y + BlockID * NumberOfPassengers);
		Numeric* t_nearest_distance = (Numeric *) (nearest_distance + BlockID * NumberOfPassengers);
		int* t_IDs = (int *) (IDs + BlockID * NumberOfPassengers);
		int* t_nearest_ID = (int *) (nearest_ID + BlockID * NumberOfPassengers);
		int* t_race_opID = (int *) (race_opID + BlockID * NumberOfPassengers);
		int* t_race_finished = (int *) (race_finished + BlockID * NumberOfPassengers);
		int* t_race_counter = (int *) (race_counter + BlockID * NumberOfPassengers);
		int* t_race_status = (int *) (race_status + BlockID * NumberOfPassengers);
		int* t_waitTime = (int *) (waitTime + BlockID * NumberOfPassengers);
		int* t_holds = (int *) (holds + BlockID * Total_Number_Of_Rows);
		Aim* t_aim = (Aim *) (aim + BlockID * NumberOfPassengers);
		bool* t_aisle_permission = (bool *) (aisle_permission + BlockID * Total_Number_Of_Rows);
		bool* t_has_released = (bool *) (has_released + BlockID * NumberOfPassengers);

		Spead_Mean = (max_SM - min_SM) * parameters[Param_Index*ParametersPerSimulation] + min_SM;
		Intersection_Distance_Threshold = (max_IDT - min_IDT) * parameters[Param_Index*ParametersPerSimulation + 1] + min_IDT;
		Intersection_Speed_Coefficient = (max_ISC - min_ISC) * parameters[Param_Index*ParametersPerSimulation + 2] + min_ISC;
		Aligning_Speed_Coefficient = (max_ASC - min_ASC) * parameters[Param_Index*ParametersPerSimulation + 3] + min_ASC;
		Toward_Aisle_Speed_Coefficient = (max_TASC - min_TASC) * parameters[Param_Index*ParametersPerSimulation + 4] + min_TASC;
		Release_Threshold = (max_RT - min_RT) * parameters[Param_Index*ParametersPerSimulation + 5] + min_RT;
		/*if(ID==0)
			printf("SM=%f, IDT=%f, ISC=%f, ASC=%f, TASC=%f, RT=%f\n", Spead_Mean, Intersection_Distance_Threshold, Intersection_Speed_Coefficient, Aligning_Speed_Coefficient, Toward_Aisle_Speed_Coefficient, Release_Threshold);*/
		for(int i=0; i<passengers_per_thread && ID*passengers_per_thread+i<NumberOfPassengers; ++i){
			int passenger_ID = ID * passengers_per_thread + i;
			curandState normal_state;
			curand_init(1234, passenger_ID, 0, &normal_state);
			Numeric speed = 0;
			while(speed<0.7 || speed>1.6){
				speed = curand_normal(&normal_state);
			}
			t_desiredSpeed[passenger_ID] = speed * std_dev + Spead_Mean;
		}
		if(ID==0)
			previousPassengersInPlane = NumberOfPassengers;


		while(numberOfPassengersInPlane[BlockID]>2){
			if(ID==0){
				++iteration_counter[BlockID];
			}

			for(int i=0; i<passengers_per_thread && ID*passengers_per_thread+i<NumberOfPassengers; ++i){
				int passenger_ID = ID * passengers_per_thread + i;
				curandState state;
				curand_init(1234, passenger_ID, 0, &state);
				getNearestPassenger(t_IDs[passenger_ID], Exit_X, t_aim, t_position_x, t_position_y, t_nearest_ID[passenger_ID], t_nearest_position_x[passenger_ID], t_nearest_position_y[passenger_ID], t_nearest_distance[passenger_ID]);

				__syncthreads();

				UpdatePassengers(t_IDs[passenger_ID], t_IDs, t_waitTime, t_race_status, t_aim, t_position_x, t_position_y, t_seat_position_x, t_aisle_permission, t_nearest_ID, t_nearest_position_x, t_nearest_position_y, t_nearest_distance, t_priority, obs_positions_x, obs_positions_y, t_race_opID, Aligning_Speed_Coefficient, Toward_Aisle_Speed_Coefficient, t_velocity_x, t_velocity_y, t_force_x, t_force_y, Reaction_Time, Intersection_Speed_Coefficient, Intersection_Distance_Threshold, Exit_X, t_desiredSpeed, Time_Step, Aligning_Stop_Threshold, Toward_Aisle_Stop_Threshold, In_Aisle_Stop_Threshold, Cutoff_Threshold);

		  /*DEBUG:
		  //cout<<endl;
			//for(int i=0; i<NumberOfPassengers; ++i){
				if((ID==132 || ID==134 || ID==135) && t_aim[passenger_ID]!= OUT){
					printf("%d: position(%f, %f) & nearest in (%f, %f) with distance %f is %d & force(%f, %f) & velocity(%f, %f) & t_aim:%d, Race: %d, Wait: %f, t_priority: %f\n", ID, t_position_x[passenger_ID], t_position_y[passenger_ID], t_nearest_position_x[passenger_ID], t_nearest_position_y[passenger_ID], t_nearest_distance[passenger_ID], t_nearest_ID[passenger_ID], t_force_x[passenger_ID], t_force_y[passenger_ID], t_velocity_x[passenger_ID], t_velocity_y[passenger_ID], t_aim[passenger_ID], t_race_status[passenger_ID], t_waitTime[passenger_ID], t_priority[passenger_ID]);
					if(t_race_status[passenger_ID]!=NO_RACE){
						printf("%d: Opponent: %d, counter: %d\n", ID, t_race_opID[passenger_ID], t_race_counter[passenger_ID]);
					}else{
						printf("\n");
					}
				}
		   // }/**/

				raceDetection(t_IDs[passenger_ID], (float *) t_position_x, (float *) t_position_y, t_velocity_x, t_velocity_y, t_force_x, t_force_y, t_race_status, t_aim, t_race_counter, t_race_opID, (float *) t_race_position_x, (float *)t_race_position_y, t_race_finished, t_nearest_ID, t_nearest_distance, state, t_waitTime, t_seat_position_x, t_aisle_permission, t_priority, Time_Step);

				UpdatePassengersFlags(t_IDs[passenger_ID], t_aim, t_position_x, t_position_y, Luggage_Y, t_waitTime, t_has_released, t_seat_position_x, t_holds, t_priority, t_aisle_permission, Exit_X, Exit_Y, t_race_status, t_race_opID, t_race_finished, t_race_position_x, t_race_position_y, Time_Step, numberOfPassengersInPlane[BlockID], Release_Threshold, state);

				t_force_x[passenger_ID] = (Numeric)0.0f;
				t_force_y[passenger_ID] = (Numeric)0.0f;
				t_nearest_distance[passenger_ID] = (Numeric)100.0f;

		}//END FOR
		if(Outputindex%int(Output_Iteration)==Output_Iteration-2 && ID==0){
			savePositions(t_position_x, t_position_y, this_output, Outputindex/Output_Iteration);
			//WriteOutput(position_x, t_position_y, 1+Outputindex/Output_Iteration, xyz_obs);
			if(Outputindex%10000==Output_Iteration-2){
				if(previousPassengersInPlane<NumberOfPassengers && previousPassengersInPlane == numberOfPassengersInPlane[BlockID]){
					printf("%d STUCK, remaining: %d\n", BlockID, numberOfPassengersInPlane[BlockID]);
					numberOfPassengersInPlane[BlockID] = 0;
				}
				previousPassengersInPlane = numberOfPassengersInPlane[BlockID];
			}
		}
		Outputindex++;
		if(Outputindex>Output_Iteration*(Number_Of_Outputs-1)){
			break;
		}
	}//End While
	/*if(ID==0)
		printf("Inside Block %d: %f, %f\n", BlockID, this_output[0], this_output[1]);*/
}


void ReadParametersFile(char* filename, Numeric* parameters, const int& n){
	stringstream seq_buff_stream;
	ifstream seq_file(filename);
	int index=0;
	if(seq_file.is_open()){
		seq_buff_stream << seq_file.rdbuf();
		seq_file.close();
	}else{
		cout<<"Error in openning sequences.txt!\n";
	}
	string seq_line;
	while(getline(seq_buff_stream, seq_line)){
		vector<string> params;
		split(seq_line, ' ', params);
		for(int i=0; i<n; ++i){
			parameters[index++] = atof(params[i].c_str());
		}
	}
}

int Simulate(){
	double time1 = microtime();
	Passengers passengers;
	Obstacles obstacles;

	/*****************************************************************************
							NO NEED TO BE IN DEVCIE
	******************************************************************************/
	char* xyz_obs;
	if(cudaMallocManaged((void**)&xyz_obs, 29500 * sizeof(char)) != cudaSuccess)
		printf("Mem Aloc Error xyz_obs\n");
	/*****************************************************************************
							NO NEED TO BE IN DEVCIE
	******************************************************************************/
	vector<Numeric> parameters;
	vector<pair<int, int> > arrangement;
	allocateMem(passengers, obstacles);
	Initialize(passengers, obstacles, xyz_obs, parameters, arrangement);

	int index=0, *iteration_counter;
	Numeric *Luggage_Y, *Exit_Y, *Aligning_Stop_Threshold, *Toward_Aisle_Stop_Threshold, *In_Aisle_Stop_Threshold, *Reaction_Time, *Time_Step, *Output_Iteration, *Number_Of_Outputs;
	Numeric *Exit_X, *Intersection_Distance_Threshold, *Intersection_Speed_Coefficient, *Aligning_Speed_Coefficient, *Toward_Aisle_Speed_Coefficient, *Release_Threshold;
	if(cudaMallocManaged((void**)&iteration_counter, NumberOfBlocks*sizeof(int)) != cudaSuccess)
		printf("Mem Aloc Error iteration_counter\n");
	if(cudaMallocManaged((void**)&Exit_X, sizeof(Numeric)) != cudaSuccess)
		printf("Mem Aloc Error Exit_X\n");
	if(cudaMallocManaged((void**)&Luggage_Y, sizeof(Numeric)) != cudaSuccess)
		printf("Mem Aloc Error Luggage_Y\n");
	if(cudaMallocManaged((void**)&Exit_Y, sizeof(Numeric)) != cudaSuccess)
		printf("Mem Aloc Error Exit_Y\n");
	if(cudaMallocManaged((void**)&Intersection_Distance_Threshold, sizeof(Numeric)) != cudaSuccess)
		printf("Mem Aloc Error Intersection_Distance_Threshold\n");
	if(cudaMallocManaged((void**)&Intersection_Speed_Coefficient, sizeof(Numeric)) != cudaSuccess)
		printf("Mem Aloc Error Intersection_Speed_Coefficient\n");
	if(cudaMallocManaged((void**)&Aligning_Speed_Coefficient, sizeof(Numeric)) != cudaSuccess)
		printf("Mem Aloc Error Aligning_Speed_Coefficient\n");
	if(cudaMallocManaged((void**)&Toward_Aisle_Speed_Coefficient, sizeof(Numeric)) != cudaSuccess)
		printf("Mem Aloc Error Toward_Aisle_Speed_Coefficient\n");
	if(cudaMallocManaged((void**)&Release_Threshold, sizeof(Numeric)) != cudaSuccess)
		printf("Mem Aloc Error Release_Threshold\n");
	if(cudaMallocManaged((void**)&Reaction_Time, sizeof(Numeric)) != cudaSuccess)
		printf("Mem Aloc Error Reaction_Time\n");
	if(cudaMallocManaged((void**)&Time_Step, sizeof(Numeric)) != cudaSuccess)
		printf("Mem Aloc Error Time_Step\n");

	if(cudaMallocManaged((void**)&Aligning_Stop_Threshold, sizeof(Numeric)) != cudaSuccess)
		printf("Mem Aloc Error Aligning_Stop_Threshold\n");
	if(cudaMallocManaged((void**)&Toward_Aisle_Stop_Threshold, sizeof(Numeric)) != cudaSuccess)
		printf("Mem Aloc Error Toward_Aisle_Stop_Threshold\n");
	if(cudaMallocManaged((void**)&In_Aisle_Stop_Threshold, sizeof(Numeric)) != cudaSuccess)
		printf("Mem Aloc Error In_Aisle_Stop_Threshold\n");
	if(cudaMallocManaged((void**)&Output_Iteration, sizeof(Numeric)) != cudaSuccess)
		printf("Mem Aloc Error Output_Iteration\n");
	if(cudaMallocManaged((void**)&Number_Of_Outputs, sizeof(Numeric)) != cudaSuccess)
		printf("Mem Aloc Error Number_Of_Outputs\n");


	/*****************************************************************************/
	/******************These should be moved to be parameters ********************/

	Intersection_Distance_Threshold[0]     = 0.8f;
	Intersection_Speed_Coefficient[0]      = 0.6f;
	Aligning_Speed_Coefficient[0]          = 0.8f;
	Toward_Aisle_Speed_Coefficient[0]      = 0.5f;
	Release_Threshold[0]                   = 1.0f;

	Luggage_Y[0] = parameters[index++];
	Exit_X[0] = parameters[index++];
	Exit_Y[0] = parameters[index++];
	Aligning_Stop_Threshold[0] = parameters[index++];
	Toward_Aisle_Stop_Threshold[0] = parameters[index++];
	In_Aisle_Stop_Threshold[0] = parameters[index++];
	Reaction_Time[0]=parameters[index++];
	Time_Step[0]=parameters[index++];
	Output_Iteration[0] = parameters[index++];
	Number_Of_Outputs[0] = parameters[index];

	int* numberOfPassengersInPlane;
	if(cudaMallocManaged((void**)&numberOfPassengersInPlane, NumberOfBlocks* sizeof(int)) != cudaSuccess)
		printf("Mem Aloc Error numberOfPassengersInPlane\n");
	for(int kk=0; kk<NumberOfBlocks; ++kk)
		numberOfPassengersInPlane[kk]= NumberOfPassengers;


	int* Total_Number_Of_Rows;
	if(cudaMallocManaged((void**)&Total_Number_Of_Rows, sizeof(int)) != cudaSuccess)
		printf("Mem Aloc Error Total_Number_Of_Rows\n");
	Total_Number_Of_Rows[0]=0;
	for (int i = arrangement.size()-1; i>=0 ; --i){
		Total_Number_Of_Rows[0]+=arrangement[i].first;
	}
	int *holds;
	bool *aisle_permission;

	if(cudaMallocManaged((void**)&holds, Total_Number_Of_Rows[0] * NumberOfBlocks * sizeof(int)) != cudaSuccess)
		printf("Mem Aloc Error holds\n");
	if(cudaMallocManaged((void**)&aisle_permission, Total_Number_Of_Rows[0] * NumberOfBlocks * sizeof(bool)) != cudaSuccess)
		printf("Mem Aloc Error aisle_permission\n");


	InitializePermissions(holds, aisle_permission, arrangement);

	Numeric* params;
	char filename[32] = "sequences.txt";
	const int Total_Number_Of_Simulations = NumberOfBlocks;

	if(cudaMallocManaged((void**)&params,  Total_Number_Of_Simulations * ParametersPerSimulation * sizeof(Numeric)) != cudaSuccess)
		printf("Mem Aloc Error params\n");
	ReadParametersFile(filename, params, ParametersPerSimulation);



	clock_t begin = clock();



	Numeric *d_outputs;
	Numeric *h_outputs =(Numeric *) malloc(550*2* NumberOfPassengers* NumberOfBlocks* sizeof(Numeric));
	memset(h_outputs, 0,  550*2*NumberOfPassengers* NumberOfBlocks*sizeof(Numeric));
	int *SimIDs;
	if(cudaMallocManaged((void**)&SimIDs, GPU_Sims * sizeof(int)) != cudaSuccess)
		printf("Mem Aloc Error SimIDs\n");

	int sim_index=0;
	for(int i=0; i<GPU_Sims; ++i){
		SimIDs[sim_index++]=i + 0;	
	}
	if(cudaMalloc((void**)&d_outputs, 550 * 2 * NumberOfPassengers* NumberOfBlocks * sizeof(Numeric)) != cudaSuccess)
		printf("Mem Aloc Error outputs\n");
	cudaMemset(d_outputs, 0, 550*2*NumberOfPassengers* NumberOfBlocks*sizeof(Numeric));
	//RunWithSM<<<NumberOfBlocks, NumberOfThreads, ((2*NumberOfObstacles + 15*NumberOfPassengers + 13)*sizeof(Numeric) + (7 * NumberOfPassengers + 4 + Total_Number_Of_Rows[0])*sizeof(int) + (NumberOfPassengers + Total_Number_Of_Rows[0])*sizeof(bool) + NumberOfPassengers*sizeof(Aim))>>>(iteration_counter, numberOfPassengersInPlane[0], Exit_X[0], Exit_Y[0], passengers.aim, passengers.position_x, passengers.position_y, passengers.nearest_ID, passengers.nearest_position_x, passengers.nearest_position_y, passengers.nearest_distance, passengers.ID, passengers.waitTime, passengers.race_status, passengers.seat_position_x, aisle_permission, passengers.priority, obstacles.positions_x, obstacles.positions_y, passengers.race_opID, passengers.mass, passengers.velocity_x, passengers.velocity_y, passengers.force_x, passengers.force_y, Reaction_Time[0], passengers.desiredSpeed, Time_Step[0], passengers.race_is_finished, passengers.race_counter, passengers.race_position_x, passengers.race_position_y, Luggage_Y[0], passengers.has_released, holds, Aligning_Stop_Threshold[0], Toward_Aisle_Stop_Threshold[0], In_Aisle_Stop_Threshold[0], Output_Iteration[0], Number_Of_Outputs[0], xyz_obs, Total_Number_Of_Rows[0], params, d_outputs);

	RunWithoutSM<<<NumberOfBlocks, NumberOfThreads>>>(iteration_counter, numberOfPassengersInPlane, Exit_X[0], Exit_Y[0], passengers.aim, passengers.position_x, passengers.position_y, passengers.nearest_ID, passengers.nearest_position_x, passengers.nearest_position_y, passengers.nearest_distance, passengers.ID, passengers.waitTime, passengers.race_status, passengers.seat_position_x, aisle_permission, passengers.priority, obstacles.positions_x, obstacles.positions_y, passengers.race_opID, passengers.velocity_x, passengers.velocity_y, passengers.force_x, passengers.force_y, Reaction_Time[0], passengers.desiredSpeed, Time_Step[0], passengers.race_is_finished, passengers.race_counter, passengers.race_position_x, passengers.race_position_y, Luggage_Y[0], passengers.has_released, holds, Aligning_Stop_Threshold[0], Toward_Aisle_Stop_Threshold[0], In_Aisle_Stop_Threshold[0], Output_Iteration[0], Number_Of_Outputs[0], Total_Number_Of_Rows[0], params, d_outputs, SimIDs);


	cudaDeviceSynchronize();
	cudaMemcpy(h_outputs, d_outputs, 550*2* NumberOfPassengers* NumberOfBlocks*sizeof(Numeric) , cudaMemcpyDeviceToHost);
	/*for(int s=0; s<NumberOfBlocks; ++s)
		cout<<"Block "<<s<<": "<<h_outputs[s*550*2*NumberOfPassengers]<<" , "<<h_outputs[s*550*2*NumberOfPassengers+1]<<endl;*/
	WriteToBinaryFile(h_outputs, xyz_obs, Time_Step[0], Output_Iteration[0], iteration_counter);

	clock_t end = clock();
	double elapsed_secs = double(end - begin) / CLOCKS_PER_SEC;
	cout<<"Elapsed Time: "<<elapsed_secs <<"(s)"<<endl;
	//cout<<"Physical time: "<<(Numeric)(iteration_counter[0]*Time_Step[0])<<" (s) = "<<(Numeric)(iteration_counter[0]*Time_Step[0])/60.0f<<" minutes"<<endl;
	freeMem(passengers, obstacles, d_outputs);
	CheckCUDAError("Kernel Free Mem: ");
	free(h_outputs);
	double time2 = microtime();

	printf("\nKernel time = %g seconds\n", (time2-time1)/1.0e6);
	return 0;
}


