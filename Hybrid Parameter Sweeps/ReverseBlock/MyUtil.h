/*
void split(const string &s, char delim, vector<string> &elems);
double    microtime(void);
double    get_microtime_resolution(void);
void master(char *filename);
void slave();
void CALM(Numeric sm, Numeric idt, Numeric isc, Numeric asc, Numeric tasc, Numeric rt, int ID);
*/

#ifndef MyUtil_h
#define MyUtil_h

#include <sys/time.h>
#include <sstream>
#include <string>
#include <vector>
#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <array>
#include <cmath>
#include <chrono>
#include <sstream>
#include <iterator>
#include <map>
#include <iomanip>
#include <time.h>
#include <stdio.h>
#include <random>
#define ParameterSweepSize 8000
#define CPU_Step 1700
#define GPU_Step 6300
#define CPU_Sims 1700
#define GPU_Sims 6300
using namespace std;

int MPI_Run(int argc, char *argv[]);

void split(const string &s, char delim, vector<string> &elems);
double    microtime(void);
double    get_microtime_resolution(void);


#endif
